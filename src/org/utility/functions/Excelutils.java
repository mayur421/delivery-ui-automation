package org.utility.functions;


	import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

	import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;

	public class Excelutils {
		public static XSSFSheet ExcelWSheet;
		 
		public static XSSFWorkbook ExcelWBook;
		
		public static XSSFCell Cell;

		public static XSSFRow Row;
		
		public static  void setExcelFile(String Path,String SheetName) throws Exception {
			 
				try {

	   			// Open the Excel file

				FileInputStream ExcelFile = new FileInputStream(Path);

				// Access the required test data sheet			 
					  ExcelWBook = new XSSFWorkbook(ExcelFile);
					  ExcelWSheet = ExcelWBook.getSheet(SheetName);
								 
					} catch (Exception e){

					throw (e);

				}

		}

	    public static String getCellData(int RowNum, int ColNum) throws Exception{

				try{
					
					
	  			Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
	             			
	  			String CellData = Cell.getStringCellValue();

	  			return CellData;

	  			}catch (Exception e){

	  				e.printStackTrace();
	  				return"";
					

	  			}

	    }
	    static public int getRowCount()
	    {
	    	return ExcelWSheet.getLastRowNum();
	    }
	    @SuppressWarnings("deprecation")
		public static void setCellData(String File_TestData,String Result, int RowNum, int ColNum) throws Exception	{
	    	 
				try{
					XSSFCellStyle style = ExcelWBook.createCellStyle();
	    			style.setFillForegroundColor(new XSSFColor(new java.awt.Color(246,0,0)));
	    			style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
	    			
	  			Row  = ExcelWSheet.getRow(RowNum);

				Cell = Row.getCell(ColNum, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);

				if (Cell == null) {

					Cell = Row.createCell(ColNum);

					Cell.setCellValue(Result);

					} else {

						Cell.setCellValue(Result);

					}


				FileOutputStream fileOut = new FileOutputStream(Constant.Path_TestData + File_TestData);
				 
					ExcelWBook.write(fileOut);
					Cell.setCellStyle(style);

					fileOut.flush();

					fileOut.close();

				}catch(Exception e){

					throw (e);

			}

	    } 
	    public static void Execute(WebDriver dr,Properties pro,String path) throws Exception{
	    	 
			//This is to get the values from Excel sheet, passing parameters (Row num &amp; Col num)to getCellData method

			//String sUserName = Excelutils.getCellData(1, 1);

			//String sPassword = Excelutils.getCellData(1, 2);

			  
	}
	 /*   @Test
	    public void test()
	    {
	    		try{
	    			Excelutils.setExcelFile("C:\\Users\\mayur.gupta\\workspace\\CL2.0\\ExcelSheets\\Assignplayoutid.xlsx", "Sheet1");
	    			XSSFCellStyle style = ExcelWBook.createCellStyle();
	    			style.setFillForegroundColor(new XSSFColor(new java.awt.Color(128, 0, 128)));
	    			style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
	    			

	    			}
	    		catch(Exception e)
	    		{
	    			e.printStackTrace();
	    			System.out.println("Not able to select Date");	
	    		}
	    	}*/
	}

