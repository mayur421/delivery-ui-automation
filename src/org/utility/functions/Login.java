package org.utility.functions;

	import java.io.FileInputStream;
	import java.io.IOException;
	import java.util.Properties;

	import org.apache.log4j.Logger;
	import org.browsersetup.Browsersetup;
	import org.openqa.selenium.By;
	import org.openqa.selenium.WebDriver;

	import com.relevantcodes.extentreports.LogStatus;

	public class Login {
		static Logger log;
		static Properties pro;
		static WebDriver dr;
		static String scrrenpasspath = Constant.scrrenpasspaths;

		public static void logintoportal(WebDriver dr, String username,
				String password, String path) throws IOException,
				InterruptedException {
			FileInputStream fle = new FileInputStream(path);
			pro = Browsersetup.loadProperties(fle);
			log = Browsersetup.logger1();
			// Enter User name
			// Calendar c = Calendar.getInstance();
			// int hour=c.get(Calendar.HOUR);
			// int min=c.get(Calendar.MINUTE);

			dr.findElement(By.id(pro.getProperty("usrnmxpath"))).sendKeys(username);
			Extentreportutils.starttest("Login",
					"Check whether user is able to login");
			Extentreportutils.log(LogStatus.PASS, "Enter Username",
					"To check whether user is able to enter Username", "");
			// Enter password

			dr.findElement(By.id(pro.getProperty("psswdxpath"))).sendKeys(password);
			Extentreportutils.log(LogStatus.PASS, "Enter Password",
					"To check whether user is able to enter Password", "");
			Thread.sleep(2000);
			// Click on Sign in
			dr.findElement(By.xpath(pro.getProperty("loginbutton"))).click();
			Thread.sleep(2000);
			// checking whether user successfully logged in
			if (dr.findElement(By.xpath(pro.getProperty("Loggedinstatus")))
					.isDisplayed()) {
				System.out.println("Login success");
				log.info("-----------------------------Login Successfull-----------------------------");
				Takescreenshots.screenshots(dr, "Loginsuccess");

				Extentreportutils.log(LogStatus.PASS, "Click Signin Button",
						"To check whether user is able to Login", scrrenpasspath
								+ "Loginsuccess.png");
				// Extentreportutils.endtest();
			} else {
				System.out.println("Login unsuccessfull");
				log.info("-----------------------------Login Unsuccesfull-----------------------------");

				Takescreenshots.screenshots(dr, "LoginFail");
				Extentreportutils.log(LogStatus.FAIL, "Click Signin Button",
						"To check whether user is able to Login", scrrenpasspath
								+ "LoginFail.png");
				Extentreportutils.endtest();
			}
		}

		public static void logout(WebDriver dr, Logger log, Properties pro,
				String applogout) throws IOException, InterruptedException {
			try {
				// Extentreportutils.e
				Constant.clickxpath(dr, applogout);
				Thread.sleep(3000);
				if (!dr.findElement(By.xpath(pro.getProperty("Loggedinstatus")))
						.isDisplayed()) {
					Thread.sleep(2000);
					Takescreenshots.screenshots(dr, "Logoutsuccess123");
					// Extentreportutils.log(LogStatus.PASS,
					// "Verify Logout","To check whether user is logged out",
					// scrrenpasspath+"Logoutsuccess123.png");
					System.out.println("Logout Successfull");
					log.info("-----------------------------Logout Successfull-----------------------------");
					Thread.sleep(2000);
					dr.close();
					dr.quit();

				} else {
					Thread.sleep(2000);
					Takescreenshots.screenshots(dr, "Logoutfail");
					// Extentreportutils.log(LogStatus.FAIL,
					// "Verify Logout","To check whether user is logged out",
					// scrrenpasspath+"Logoutfail.png");
					System.out.println("Logout Unsuccessfull");
					Thread.sleep(2000);
					log.info("-----------------------------Logout Unsuccessfull-----------------------------");
					dr.quit();
				}
			} catch (Exception e) {

				e.printStackTrace();
				System.out.println("User not able to log out");
				log.info("User not able to logout");
				dr.quit();
			}

		}

	}



