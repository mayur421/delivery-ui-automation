package org.utility.functions;


	import java.io.File;
	import java.util.Calendar;

	import com.relevantcodes.extentreports.ExtentReports;
	import com.relevantcodes.extentreports.LogStatus;

	public class Extentreportutils {

		static ExtentReports extent = ExtentReports.get(Extentreportutils.class);
		static String reportpath = "C:\\Users\\mayur.gupta\\Workspace\\Swiggy\\test-output\\ExtentReports\\";

		static {

			// static Class<Extentreportutils> extent = Extentreportutils.class;
			Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int date = c.get(Calendar.DATE);
			int hour = c.get(Calendar.HOUR);
			int min = c.get(Calendar.MINUTE);
			new File(reportpath + "Results" + " " + date + " " + month + " " + year
					+ "  " + hour + " " + min + ".html");
			extent = new ExtentReports();
			extent.init(reportpath + "Results" + " " + date + " " + month + " "
					+ year + "  " + hour + " " + min + ".html", true);
			extent.config().documentTitle("Swiggy-Delivery");
		}

		public static void starttest(String testname, String description) {
			extent.startTest(testname, description);
		}

		public static void endtest() {
			extent.endTest();
		}

		public static void log(LogStatus logstatus, String stepname,
				String details, String attachment) {
			if (attachment != "") {
				extent.log(logstatus, stepname, details, attachment);

			} else {
				extent.log(logstatus, stepname, details, attachment);
			}

		}

	}



