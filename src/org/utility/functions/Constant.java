package org.utility.functions;

	import org.openqa.selenium.By;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.support.ui.Select;

	public class Constant {

		public static final String URL = "http://www.store.demoqa.com";

		public static final String Username = "testuser_1";

		public static final String Password = "Test@123";
		public static String objpath = "C:\\Users\\mayur.gupta\\Workspace\\Swiggy\\src\\properties\\files\\object_repository_DE";
		public static String objpathzone = "C:\\Users\\mayur.gupta\\Workspace\\Swiggy\\src\\properties\\files\\object_repository_zone";
		public static String objpathincentive = "C:\\Users\\mayur.gupta\\Workspace\\Swiggy\\src\\properties\\files\\object_repository_incentive";
		public static String objpathcashman = "C:\\Users\\mayur.gupta\\Workspace\\Swiggy\\src\\properties\\files\\object_repository_cash_management";
		public static String objpathmansurge = "C:\\Users\\mayur.gupta\\Workspace\\Swiggy\\src\\properties\\files\\object_repository_manual_surge";
		public static String objpathgenerateagree = "C:\\Users\\mayur.gupta\\Workspace\\Swiggy\\src\\properties\\files\\object_repository_generateagreement";
		public static String objpathoperations="C:\\Users\\mayur.gupta\\Workspace\\Swiggy\\src\\properties\\files\\object_repository_operations";
		public static String objtest = "C:\\Users\\mayur.gupta\\Workspace\\Swiggy\\src\\properties\\files\\testobj";
		public static String scrrenpasspaths = "C:\\Users\\mayur.gupta\\Workspace\\Swiggy\\Screenshots\\";
		public static final String Path_TestData = "C:\\Users\\mayur.gupta\\workspace\\CL2.0\\Testdata\\ExcelSheets\\";

		public static void clickxpath(WebDriver dr, String element) {
			dr.findElement(By.xpath(element)).click();
		}

		public static void clickid(WebDriver dr, String element) {
			dr.findElement(By.id(element)).click();
		}

		public static void sendkysxpath(WebDriver dr, String element, String data) {
			dr.findElement(By.xpath(element)).sendKeys(data);
		}

		public static void sendkysid(WebDriver dr, String element, String data) {
			dr.findElement(By.id(element)).sendKeys(data);
		}

		public static void selectbyindx_id(WebDriver dr, String element, int index) {
			Select sttmhr = new Select(dr.findElement(By.id(element)));
			sttmhr.selectByIndex(index);
		}

		public static void selectbyindx_xpath(WebDriver dr, String element,
				int index) {
			Select sttmhr = new Select(dr.findElement(By.xpath(element)));
			sttmhr.selectByIndex(index);
		}

		public static void selectbyvistxt_xpath(WebDriver dr, String element,
				String text) {
			Select sttmhr = new Select(dr.findElement(By.xpath(element)));
			sttmhr.selectByVisibleText(text);
		}

		public static void selectbyvistxt_id(WebDriver dr, String element,
				String text) {
			Select sttmhr = new Select(dr.findElement(By.id(element)));
			sttmhr.selectByVisibleText(text);
		}

		public static void clickclassname(WebDriver dr, String element) {
			dr.findElement(By.className(element)).click();
		}

	}



