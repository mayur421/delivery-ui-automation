package org.utility.functions;

	import java.io.File;
	import java.io.IOException;

	import org.apache.commons.io.FileUtils;
	import org.openqa.selenium.OutputType;
	import org.openqa.selenium.TakesScreenshot;
	import org.openqa.selenium.WebDriver;

	public class Takescreenshots {

		public static void screenshots(WebDriver dr, String fileName)
				throws IOException {
			File scrFile = ((TakesScreenshot) dr).getScreenshotAs(OutputType.FILE);
			// Now you can do whatever you need to do with it, for example copy
			// somewhere
			FileUtils.copyFile(scrFile, new File(
					"C:\\Users\\mayur.gupta\\Workspace\\Swiggy\\Screenshots\\" + fileName
							+ ".png"));
		}

	}



