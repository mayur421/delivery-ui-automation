package org.swiggy.pom;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.browsersetup.Browsersetup;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.utility.functions.Constant;

public class RainModeParams {
	WebDriver dr;
	static Properties pro;
	static String h1;
	static Logger log;
	static FileInputStream fis;
	static String path = Constant.objpath;
	static String scrrenpasspath = Constant.scrrenpasspaths;

	public RainModeParams(WebDriver dr) throws IOException {
		this.dr = dr;
		fis = new FileInputStream(Constant.objpath);
		pro = Browsersetup.loadProperties(fis);
		log = Browsersetup.logger1();
	}

	public void clickaddrainmodeparam(String element) {
		Constant.clickxpath(dr, element);

	}

	public void clearall(String element) {
		dr.findElement(By.id(element)).clear();
	}

	public void selectcity(String element, String data) {
		Constant.selectbyvistxt_id(dr, element, data);
	}

	public void entersurgefee(String element, String text) {
		Constant.sendkysid(dr, element, text);
	}

	public void enterbannermessage(String element, String data) {
		Constant.sendkysid(dr, element, data);
	}

	public void enterlastmilcap(String element, String data) {
		Constant.sendkysid(dr, element, data);
	}

	public void entermaxsla(String element, String data) {
		Constant.sendkysid(dr, element, data);
	}

	public void enterbeefuptime(String element, String data) {
		Constant.sendkysid(dr, element, data);
	}

	public void entersurgemultilpier(String element, String data) {
		Constant.sendkysid(dr, element, data);
	}

	public void entersurgemin(String element, String data) {
		Constant.sendkysid(dr, element, data);
	}

	public void clicksavebutton(String element) {
		Constant.clickxpath(dr, element);
	}

}



