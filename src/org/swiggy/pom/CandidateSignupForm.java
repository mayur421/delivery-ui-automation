package org.swiggy.pom;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.browsersetup.Browsersetup;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.utility.functions.Constant;

public class CandidateSignupForm {
	WebDriver dr;
	static Properties pro;
	static String h1;
	static Logger log;
	static FileInputStream fis;
	static String path = Constant.objpath;
	static String scrrenpasspath = Constant.scrrenpasspaths;

	public CandidateSignupForm(WebDriver dr) throws IOException {
		this.dr = dr;
		fis = new FileInputStream(Constant.objpath);
		pro = Browsersetup.loadProperties(fis);
		log = Browsersetup.logger1();
	}

	public void selectopr(String operation) throws InterruptedException,
			IOException {
		try {
			log.info("Selecting operatichon");
			WebElement ch = dr.findElement(By.xpath(pro
					.getProperty("operations")));
			ch.click();
			// Actions action= new Actions(dr);
			// action.moveToElement(ch).build().perform();
			Thread.sleep(2000);
			switch (operation) {
			case "Autoassignlogs":
				Constant.clickxpath(dr, pro.getProperty("autoassinglogsxpath"));
				log.info("Auto Assign logs selected");
				break;
			case "Candidates":
				// Thread.sleep(1000);
				Constant.clickxpath(dr, pro.getProperty("Candidatesxpath"));
				log.info("Candiates operation selected");
				System.out.println("Candiates operation selected");
				break;
			case "IncentiveRule1":
				Constant.clickxpath(dr, pro.getProperty("IncentiveRulexpath1"));
				log.info("DE incentive rule operation selected");
				break;
			case "IncentiveRule2":
				Constant.clickxpath(dr, pro.getProperty("IncentiveRulexpath2"));
				log.info("DE incentive rule 2 operation selected");
				break;
			case "ShiftTimings":
				Constant.clickxpath(dr, pro.getProperty("ShiftTimingsxpath"));
				log.info("Shift Timing operation selected");
				break;
			case "DeliveryBoys":
				Constant.clickxpath(dr, pro.getProperty("Deliveryboysxpath"));
				log.info("Delivery Boys operation selected");
				break;
			}

		} catch (Exception e) {
			log.error("operation not selected", e);

			System.out.println("OPeration not selected");

		}
	}

	public void clickaddcandidatebutton(String addcand) {
		Constant.clickxpath(dr, addcand);
	}

	public void entername(String element, String name) {
		Constant.sendkysid(dr, element, name);
	}

	public void seletcity(String element, String name) {
		Constant.selectbyvistxt_id(dr, element, name);
	}

	public void enterpersonalctnno(String element, String no) {
		Constant.sendkysid(dr, element, no);
	}

	public void selectempstatus(String element, String text) {
		Constant.selectbyvistxt_id(dr, element, text);
	}

	public void entertrainingstart(String element, String dat1) {

		Constant.sendkysid(dr, element, dat1);
	}

	public void entertrainingend(String element, String dat1) {

		Constant.sendkysid(dr, element, dat1);
	}

	public void entercurraddline1(String element, String addr1) {
		Constant.sendkysid(dr, element, addr1);
	}

	public void entercurrlandmark(String element, String land) {
		Constant.sendkysid(dr, element, land);
	}

	public void enterpostalcode(String element, String zipcode) {
		Constant.sendkysid(dr, element, zipcode);
	}

	public void copycurrtoperm(String element) {
		Constant.clickid(dr, element);
	}

	public void selectdltype(String element, String dlyp) {
		Constant.selectbyvistxt_id(dr, element, dlyp);
	}

	public void enterdlid(String element, String dlid) {
		Constant.sendkysid(dr, element, dlid);
	}

	public void enterdladd(String element, String dladd) {
		Constant.sendkysid(dr, element, dladd);
	}

	public void enterdlstate(String element, String stat) {
		Constant.sendkysid(dr, element, stat);
	}

	public void enterdlissuedon(String element, String dat) {
		Constant.sendkysid(dr, element, dat);
	}

	public void enterdlvalidfrm(String element, String dat) {
		Constant.sendkysid(dr, element, dat);
	}

	public void enterdlvalidupto(String element, String dat) {
		Constant.sendkysid(dr, element, dat);
	}

	public void enterRcno(String element, String dat) {
		Constant.sendkysid(dr, element, dat);
	}

	public void selectRctyp(String element, String dat) {
		Constant.selectbyvistxt_id(dr, element, dat);
	}

	public void enterPancardno(String element, String dat) {
		Constant.sendkysid(dr, element, dat);
	}

	public void selectPantyp(String element, String dat) {
		Constant.selectbyvistxt_id(dr, element, dat);
	}

	public void enterBikercno(String element, String dat) {
		Constant.sendkysid(dr, element, dat);
	}

	public void enterbikename(String element, String dat) {
		Constant.sendkysid(dr, element, dat);
	}

	public void selectreportarea(String element, String dat) {
		Constant.selectbyvistxt_id(dr, element, dat);
	}

	public void selectsecondaryarea(String element, String dat) {
		Constant.selectbyvistxt_id(dr, element, dat);
	}

	public void selectshifttyp(String element, String dat) {
		Constant.selectbyvistxt_id(dr, element, dat);
	}

	public void selectfullshifttyp(String element) {
		Constant.clickxpath(dr, element);
	}

	public void enteruserbankname(String element, String dat) {
		Constant.sendkysid(dr, element, dat);
	}

	public void enterbankaccountno(String element, String dat) {
		Constant.sendkysid(dr, element, dat);
	}

	public void enterbankname(String element, String dat) {
		Constant.sendkysid(dr, element, dat);
	}

	public void enterIfsccode(String element, String dat) {
		Constant.sendkysid(dr, element, dat);
	}

	public void entersecuirtydeposits(String element, String dat) {
		Constant.sendkysid(dr, element, dat);
	}

	public void enterseiggynumber(String element, String dat) {
		Constant.sendkysid(dr, element, dat);
	}

	public void clicksavebutton(String element) {
		Constant.clickxpath(dr, element);
	}
}




