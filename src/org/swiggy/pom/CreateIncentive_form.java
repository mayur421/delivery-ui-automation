package org.swiggy.pom;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.browsersetup.Browsersetup;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.utility.functions.Constant;
import org.utility.functions.Takescreenshots;

public class CreateIncentive_form {

	WebDriver dr;
	static Properties pro;
	static String h1;
	static Logger log;
	static FileInputStream fis;
	static int i;
	static String path = Constant.objpathincentive;
	static String scrrenpasspath = Constant.scrrenpasspaths;

	public CreateIncentive_form(WebDriver dr) throws IOException {

		this.dr = dr;
		fis = new FileInputStream(Constant.objpath);
		pro = Browsersetup.loadProperties(fis);
		log = Browsersetup.logger1();

	}

	public void selectcreateincetivelink(String element) {
		Constant.clickxpath(dr, element);

	}

	public void entervalidityfrom(String element, String data, String data2) {
		Constant.sendkysxpath(dr, element, data + Keys.TAB + data2);

	}

	public void entervalidityto(String element, String data, String data2) {
		Constant.sendkysxpath(dr, element, data + Keys.TAB + data2);
	}

	public void enterincentivecode(String element, String data) {
		Constant.sendkysxpath(dr, element, data);
	}

	public void enterincentivedescription(String element, String data) {
		Constant.sendkysxpath(dr, element, data);
	}

	public void enterbonusamount(String element, String data) {
		Constant.sendkysid(dr, element, data);
	}

	public void enterincentivestarttimeslot(String element, String data) {
		Constant.sendkysxpath(dr, element, data);
	}

	public void enterincentiveendtimeslot(String element, String data) {
		Constant.sendkysxpath(dr, element, data);
	}

	public void unselectincentivedays(String day) {
		switch (day) {
		case "MONDAY":
			i = 1;
			break;
		case "TUESDAY":
			i = 2;
			break;
		case "WEDNESDAY":
			i = 3;
			break;
		case "THURSDAY":
			i = 4;
			break;
		case "FRIDAY":
			i = 5;
			break;
		case "SATURDAY":
			i = 6;
			break;
		case "SUNDAY":
			i = 7;
			break;
		}
		dr.findElement(
				By.xpath(".//td[@class='c_dayschecked']/label[" + i + "]/input"))
				.click();

	}

	public void selecteachcondition(String value, String element) {
		switch (value) {
		case "Hourly":
			Constant.selectbyvistxt_xpath(dr, element, "Hourly");
			break;
		case "Order":
			Constant.selectbyvistxt_xpath(dr, element, "Order");
			break;
		case "Rating":
			Constant.selectbyvistxt_xpath(dr, element, "Customer Rating");
			Constant.sendkysxpath(dr, ".//input[@name='e_conditionx_1']", "1.5");
			Constant.sendkysxpath(dr, ".//input[@name='e_conditionx_2']", "3.0");
			break;
		}
	}

	public void selectcumulativetimeconditon(String element, String value1,
			String value2) throws IOException {
		// Selecting Time condition
		Constant.selectbyvistxt_xpath(dr, element, value1);
		// Selecting Cumulative condition
		WebElement element1 = dr.findElement(By.id("submit_btn"));
		((JavascriptExecutor) dr).executeScript(
				"arguments[0].scrollIntoView(true);", element1);

		switch (value2) {
		case "Order":
			dr.findElement(
					By.xpath(".//div[@id='cumulative_conditions']/input[@id='order_cumulative_condition']"))
					.click();
			dr.findElement(By.xpath(".//input[@name='c_conditionx_3']"))
					.sendKeys("5");
			break;
		case "Max Rejects":
			dr.findElement(
					By.xpath(".//div[@id='cumulative_conditions']/input[@id='acceptance_cumulative_condition']"))
					.click();
			dr.findElement(By.xpath(".//input[@name='c_conditionx']"))
					.sendKeys("56");
			break;
		case "Hourly":
			dr.findElement(
					By.xpath(".//div[@id='non_commitment_cumulative']/input[@id='hour_cumulative_condition']"))
					.click();
			dr.findElement(By.xpath(".//input[@name='c_conditionx_4']"))
					.sendKeys("8");
			break;
		case "Customer Rating":
			dr.findElement(
					By.xpath(".//div[@id='non_commitment_cumulative']/input[@id='rating_cumulative_condition']"))
					.click();
			dr.findElement(By.xpath(".//input[@name='c_conditionx_1']"))
					.sendKeys("1.5");
			dr.findElement(By.xpath(".//input[@name='c_conditionx_2']"))
					.sendKeys("3.0");
			break;
		case "Max Reject":
			dr.findElement(
					By.xpath(".//div[@id='non_commitment_cumulative']/input[@id='max_reject_cumulative_condition']"))
					.click();
			dr.findElement(By.xpath(".//input[@name='c_conditionx_5']"))
					.sendKeys("5");
			break;
		}
		Takescreenshots.screenshots(dr, "Created Incentive");

	}

	public void enterminimumpromiseorders(String element, String value1) {
		Constant.sendkysxpath(dr, element, value1);
	}

	public void enterminimumpromiseloggedtime(String element, String value) {
		Constant.sendkysxpath(dr, element, value);
	}

	public void enternoofpeaks(String element, String value) {
		Constant.sendkysid(dr, element, value);
	}

	public void enterminmiumloggedintime(String element, String value) {
		Constant.sendkysxpath(dr, element, value);
	}

	public void selectcommitimecondtion(String element, String value) {
		Constant.selectbyvistxt_xpath(dr, element, value);
	}

	public void entercommtiedsumulativeorders(String option, String value) {
		WebElement element1 = dr.findElement(By.id("submit_btn"));
		((JavascriptExecutor) dr).executeScript(
				"arguments[0].scrollIntoView(true);", element1);

		switch (option) {
		case "ordr":

			dr.findElement(By.id("order_cumulative_condition")).click();
			dr.findElement(By.xpath(".//input[@name='c_conditionx_3']"))
					.sendKeys(value);
			break;

		case "reject":
			dr.findElement(By.id("acceptance_cumulative_condition")).click();
			dr.findElement(By.xpath(".//input[@name='c_conditionx']"))
					.sendKeys(value);

		}
	}

	public void clicksubmitbutton(String element) {
		Constant.clickid(dr, element);
	}

}

