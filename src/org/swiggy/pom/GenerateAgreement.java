package org.swiggy.pom;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.browsersetup.Browsersetup;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.utility.functions.Constant;

public class GenerateAgreement {
	WebDriver dr;
	static Properties pro;
	static String h1;
	static Logger log;
	static FileInputStream fis;
	static int i;
	static String path = Constant.objpathgenerateagree;
	static String scrrenpasspath = Constant.scrrenpasspaths;

	public GenerateAgreement(WebDriver dr) throws IOException {

		this.dr = dr;
		fis = new FileInputStream(Constant.objpathgenerateagree);
		pro = Browsersetup.loadProperties(fis);
		log = Browsersetup.logger1();

	}

	public void clickgenerateagreelink(String element) {
		Constant.clickxpath(dr, element);
	}

	public void selectcity(String element, String data) {
		List<WebElement> ls = dr.findElements(By.className("city"));
		for (WebElement e : ls) {
			System.out.println(e.getText());
			if (e.getText().equals(data)) {
				System.out.println("Found");
				e.click();
				break;
			}
		}

		// breakloop: for (int i=1;i<=ls.size();i++)
		// {
		// if(l)
		//
		// String s=dr.findElement(By.xpath(".//input[" +i+
		// "][@class='city']")).g;
		// // ".//form[@id='agreements_form']/div[2]/input[" +i+ "]")
		// if(s.equalsIgnoreCase(data))
		// {
		// break breakloop;
		// }
		// }
		// int rw=i;
		// dr.findElement(By.xpath(".//form[@id='agreements_form']/div[2]/input["
		// +rw+ "]"));
	}

}


