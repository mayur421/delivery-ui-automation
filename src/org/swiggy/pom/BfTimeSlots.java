package org.swiggy.pom;



import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.browsersetup.Browsersetup;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.utility.functions.Constant;

public class BfTimeSlots {
	WebDriver dr;
	static Properties pro;
	static String h1;
	static Logger log;
	static FileInputStream fis;
	static String path = Constant.objpath;
	static String scrrenpasspath = Constant.scrrenpasspaths;

	public BfTimeSlots(WebDriver dr) throws IOException {
		this.dr = dr;
		fis = new FileInputStream(Constant.objpath);
		pro = Browsersetup.loadProperties(fis);
		log = Browsersetup.logger1();
	}

	public void clearall(String element) {
		dr.findElement(By.id(element)).clear();
	}

	public void clickAddbftimeslotbutton(String element) {
		Constant.clickxpath(dr, element);
	}

	public void selectbftimslotzone(String element, String data) {
		Constant.selectbyvistxt_id(dr, element, data);
	}

	public void enterbftimeslotopentime(String element, String data) {
		Constant.sendkysid(dr, element, data);
	}

	public void enterbftimeslotsclosetime(String element, String data) {
		Constant.sendkysid(dr, element, data);
	}

	public void enterbftimeslotstartorderfactor(String element, String data) {
		Constant.sendkysid(dr, element, data);
	}

	public void enterbftimeslotstoporderfactor(String element, String data) {
		Constant.sendkysid(dr, element, data);
	}

	public void clicksavebtn(String element) {
		Constant.clickid(dr, element);
	}
}




