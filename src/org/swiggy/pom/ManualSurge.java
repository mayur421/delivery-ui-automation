package org.swiggy.pom;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.browsersetup.Browsersetup;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import org.utility.functions.Constant;
import org.utility.functions.DbConnection;

public class ManualSurge {

	WebDriver dr;
	static Properties pro;
	static String h1;
	static Logger log;
	static FileInputStream fis;
	static int i;
	static String path = Constant.objpathmansurge;
	static String scrrenpasspath = Constant.scrrenpasspaths;

	public ManualSurge(WebDriver dr) throws IOException {

		this.dr = dr;
		fis = new FileInputStream(path);
		pro = Browsersetup.loadProperties(fis);
		log = Browsersetup.logger1();

	}

	public void clickmanualsurge(String element) {
		Constant.clickxpath(dr, element);
	}

	public void selectcity(String element, String data) {
		Constant.selectbyvistxt_id(dr, element, data);

	}

	public void selectarea(String element, String data) {
		Constant.selectbyvistxt_id(dr, element, data);
	}

	public void entervalidityfrom(String element, String data, String data2) {
		Constant.sendkysxpath(dr, element, data + Keys.TAB + data2);

	}

	public void entervalidityto(String element, String data, String data2) {
		Constant.sendkysxpath(dr, element, data + Keys.TAB + data2);

	}

	public void entersurgemultiplier(String element, String data) {
		Constant.sendkysxpath(dr, element, data);
	}

	public void clicksubmitbtn(String element) {
		Constant.clickxpath(dr, element);
	}

	public boolean checkmanualsurgecreated(String query, String data1,
			String data2,String data3) throws ClassNotFoundException, SQLException {
		HashMap<String, String> map = DbConnection.executeQuery(query);
		String id = map.get("id");
		String areaid = map.get("area_id");
		String multi = map.get("multiplier");
		if ((areaid.equalsIgnoreCase(data1)) && (multi.equalsIgnoreCase(data2))
				&& (id.equalsIgnoreCase(data3))) {
			return true;
		} else {
			return false;
		}

	}
}


