package org.swiggy.pom;


import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.browsersetup.Browsersetup;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.utility.functions.Constant;
import org.utility.functions.DbConnection;

public class RuleAreaMap {

	WebDriver dr;
	static Properties pro;
	static String h1;
	static Logger log;
	static FileInputStream fis;
	static int i;
	static String path = Constant.objpathincentive;
	static String scrrenpasspath = Constant.scrrenpasspaths;

	public RuleAreaMap(WebDriver dr) throws IOException {

		this.dr = dr;
		fis = new FileInputStream(Constant.objpath);
		pro = Browsersetup.loadProperties(fis);
		log = Browsersetup.logger1();

	}

	public void clickruleareamap(String element) {
		Constant.clickxpath(dr, element);
	}

	public void enterruleid(String element, String id) {
		Constant.sendkysid(dr, element, id);
	}

	public void clicksubmitbtn(String element) {
		Constant.clickxpath(dr, element);
	}

	public void selectcityformapping(String element, String data) {
		Constant.selectbyvistxt_id(dr, element, data);
	}

	public void selectareaselect(String element1, String data) {
		try {
			int rw = 0;
			List<WebElement> record = dr.findElements(By.xpath(element1));
			breakloop: for (rw = 1; rw <= record.size(); rw++) {
				String s = dr.findElement(
						By.xpath(".//div[@id='areas']/label[" + rw + "]"))
						.getText();
				Thread.sleep(1000);
				if (s.equalsIgnoreCase(" " + data)) {

					break breakloop;

				}
			}
			int rows = rw;
			boolean ch = dr.findElement(
					By.xpath(".//div[@id='areas']/label[" + rows + "]/input"))
					.isSelected();
			if (ch == true) {
				System.out.println("Area already Selected");
			} else {
				Thread.sleep(3000);
				dr.findElement(
						By.xpath(".//div[@id='areas']/label[" + rows
								+ "]/input")).click();
			}
			// .//div[@id='areas']/label
		}

		catch (Exception e) {
			System.out.println(e);
			System.out.println("Not able to select area for rule mapping");

		}
	}

	public void selectshiftselect(String element1, String data) {
		try {
			int rw = 0;
			List<WebElement> record = dr.findElements(By.xpath(element1));
			breakloop: for (rw = 1; rw <= record.size(); rw++) {
				String s = dr.findElement(
						By.xpath(".//div[@id='shifts']/label[" + rw + "]"))
						.getText();
				if (s.equalsIgnoreCase(" " + data)) {

					break breakloop;

				}
			}
			int rows = rw;
			boolean ch = dr.findElement(
					By.xpath(".//div[@id='shifts']/label[" + rows + "]/input"))
					.isSelected();
			if (ch == true) {
				System.out.println("Shift already Selected");
			} else {
				Thread.sleep(3000);
				dr.findElement(
						By.xpath(".//div[@id='shifts']/label[" + rows
								+ "]/input")).click();
			}
			// .//div[@id='areas']/label
		}

		catch (Exception e) {
			System.out.println(e);
			System.out.println("Not able to select area for rule mapping");

		}
	}

	public void clicksavebutton(String element) {
		Constant.clickxpath(dr, element);
	}

	public boolean checkruleareamapped(String query, String data1, String data2)
			throws ClassNotFoundException, SQLException {
		HashMap<String, String> map = DbConnection.executeQuery(query);
		String areaid = map.get("area_id");
		String ruleid = map.get("rule_id");
		if ((areaid.equalsIgnoreCase(data1))
				&& (ruleid.equalsIgnoreCase(ruleid))) {
			return true;
		} else {
			return false;
		}

	}

}




