package org.swiggy.pom;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.browsersetup.Browsersetup;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.utility.functions.Constant;

public class DeOperations {

	WebDriver dr;
	static Properties pro;
	static Logger log;
	static FileInputStream fis;
	static String path = Constant.objpathoperations;
	
	public DeOperations(WebDriver dr) throws IOException
	{
		this.dr = dr;
		fis= new FileInputStream(Constant.objpathoperations);
		pro=Browsersetup.loadProperties(fis);
		log=Browsersetup.logger1();
		
		}
	public void clickoperation(String opr)
	{
		
		dr.findElement(By.xpath(".//div[@id='left-nav']/ul/li[2]")).click();
		switch(opr)
		{
		case "autoassign":
			dr.findElement(By.xpath(".//div[@id='left-nav']/ul/li[2]/ul/li[1]")).click();
			break;
		case "candidates":
			dr.findElement(By.xpath(".//div[@id='left-nav']/ul/li[2]/ul/li[2]")).click();
			break;
		case "deincentiverule1":
			dr.findElement(By.xpath(".//div[@id='left-nav']/ul/li[2]/ul/li[3]")).click();
			break;
		case "deincentiverule2":
			dr.findElement(By.xpath(".//div[@id='left-nav']/ul/li[2]/ul/li[4]")).click();
			break;
		case "deshifttimings":
			dr.findElement(By.xpath(".//div[@id='left-nav']/ul/li[2]/ul/li[5]")).click();
			break;
		case "deliveryboys":
			dr.findElement(By.xpath(".//div[@id='left-nav']/ul/li[2]/ul/li[6]")).click();
			break;
		case "deliveryconfig":
			dr.findElement(By.xpath(".//div[@id='left-nav']/ul/li[2]/ul/li[7]")).click();
			break;
		case "incentiveauditlog":
			dr.findElement(By.xpath(".//div[@id='left-nav']/ul/li[2]/ul/li[8]")).click();
			break;
		case "inventoryitem":
			dr.findElement(By.xpath(".//div[@id='left-nav']/ul/li[2]/ul/li[9]")).click();
			break;
		case "manualsurge1":
			dr.findElement(By.xpath(".//div[@id='left-nav']/ul/li[2]/ul/li[10]")).click();
			break;
		case "manualsurge2":
			dr.findElement(By.xpath(".//div[@id='left-nav']/ul/li[2]/ul/li[11]")).click();
			break;
		case "offlineopsreport":
			dr.findElement(By.xpath(".//div[@id='left-nav']/ul/li[2]/ul/li[12]")).click();
			break;
		case "restaurant":
			dr.findElement(By.xpath(".//div[@id='left-nav']/ul/li[2]/ul/li[13]")).click();
			break;
		case "trips":
			dr.findElement(By.xpath(".//div[@id='left-nav']/ul/li[2]/ul/li[14]")).click();
			break;
		default:
			dr.findElement(By.xpath(".//div[@id='left-nav']/ul/li[2]/ul/li[1]")).click();
		}
		
	}
	
	public void autoassignselectzone(String element,String data)
	{
		Constant.selectbyvistxt_xpath(dr, element, data);
	}
	public void autoassignselectparameter(String element,String data)
	{
		Constant.selectbyvistxt_xpath(dr, element, data);
	}
	public void clicksearchbtn(String element)
	{
		Constant.clickxpath(dr, element);
	}
	public int autoassigngetrowcount(String element)
	{
		List <WebElement> ls=dr.findElements(By.xpath(element));
		int x=ls.size();
		return (x-1);
		
	}
	public void clickDeincentivelogentry(String element)
	{
	List<WebElement> ls=dr.findElements(By.xpath(element));
	for(WebElement e:ls)
	{
		e.click();
		break;
	}
	}
	public HashMap<String,String> getautoassigndetails(String element1,String element2)
	{
		HashMap<String, String> map = new HashMap<String, String>();
		List<WebElement> fields=dr.findElements(By.xpath(element1));
		List<WebElement> values=dr.findElements(By.xpath(element2));
		for (int i=1;i<=fields.size();i++)
		{
			map.put(fields.get(i).toString(),values.get(i).toString());
		}
		return map;
		
	}
}


