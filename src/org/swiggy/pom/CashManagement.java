package org.swiggy.pom;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.browsersetup.Browsersetup;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import org.utility.functions.Constant;
import org.utility.functions.DbConnection;

public class CashManagement {
	WebDriver dr;
	static Properties pro;
	static String h1;
	static Logger log;
	static FileInputStream fis;
	static int i;
	static String path = Constant.objpathcashman;
	static String scrrenpasspath = Constant.scrrenpasspaths;

	public CashManagement(WebDriver dr) throws IOException {

		this.dr = dr;
		fis = new FileInputStream(path);
		pro = Browsersetup.loadProperties(fis);
		log = Browsersetup.logger1();

	}

	public void clickcashmanagemenlink(String element) {
		Constant.clickxpath(dr, element);
	}

	public void enterdeid(String element, String data) {
		Constant.sendkysid(dr, element, data);
	}

	public void clicksessionbutton(String element) {
		Constant.clickid(dr, element);
	}

	public boolean checksession(String element) {
		String s = dr.findElement(By.xpath(element)).getText();
		if (s.equalsIgnoreCase("In Progress")) {

			return true;
		} else {
			return false;
		}
	}

	public void selectsession(String element) {
		Constant.clickxpath(dr, element);
	}

	public void clickcreatesessionbtn(String element) {
		Constant.clickxpath(dr, element);
	}

	public void entersessionname(String element, String str) {
		Constant.sendkysxpath(dr, element, str);
	}

	public void clickcreatesession(String element) {
		Constant.clickid(dr, element);
	}

	public void clickaddmanualtransactionbtn(String element) {
		Constant.clickxpath(dr, element);
	}

	public void selecttranstype(String option) throws InterruptedException {
		try {
			switch (option) {
			case "Disburse cash":
				dr.findElement(
						By.xpath(".//tr[@id='add_txn_modal_txn_type_row']/td/input[1]"))
						.click();
				break;
			case "Collect cash":
				dr.findElement(
						By.xpath(".//tr[@id='add_txn_modal_txn_type_row']/td/input[2]"))
						.click();
				break;
			case "Order related transaction":
				dr.findElement(
						By.xpath(".//tr[@id='add_txn_modal_txn_type_row']/td/input[3]"))
						.click();
				break;
			}
		} catch (Exception e) {
			System.out.println(e);

		}

	}

	public void enterdeamount(WebElement element, String data) {
		Actions actions = new Actions(dr);
		actions.moveToElement(element);
		actions.click();
		actions.sendKeys(data);
		actions.build().perform();
	}

	public void enterdescription(WebElement element, String data) {
		Actions actions = new Actions(dr);
		actions.moveToElement(element);
		actions.click();
		actions.sendKeys(data);
		actions.build().perform();

	}

	public void enterorderid(String element, String data) {
		Constant.sendkysxpath(dr, element, data);
	}

	public void clickaddtrnasbtn(String element) {
		Constant.clickid(dr, element);
	}

	public boolean checktransadded(String query, String data1, String data2)
			throws ClassNotFoundException, SQLException {
		HashMap<String, String> map1 = DbConnection.executeQuery(query);
		String deid = map1.get("de_id");
		String transsource = map1.get("transaction_source");
		System.out.println(deid + " " + transsource);
		if (deid.equalsIgnoreCase(data1) && transsource.equalsIgnoreCase(data2)) {
			return true;

		} else {
			return false;
		}
	}
}

