package org.swiggy.pom;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.browsersetup.Browsersetup;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.utility.functions.Constant;
import org.utility.functions.Takescreenshots;

public class Addzone_form {
	WebDriver dr;
	static Properties pro;
	static String h1;
	static Logger log;
	static FileInputStream fis;
	static String scrrenpasspath = Constant.scrrenpasspaths;

	public Addzone_form(WebDriver dr) throws IOException {
		this.dr = dr;
		fis = new FileInputStream(Constant.objpathzone);
		pro = Browsersetup.loadProperties(fis);
		log = Browsersetup.logger1();
	}

	public void selectzoneopr(String actionn) throws InterruptedException,
			IOException {
		try {
			log.info("Selecting zones");
			Thread.sleep(2000);
			WebElement ch = dr.findElement(By.xpath(pro
					.getProperty("operations")));
			ch.click();

			// Actions action= new Actions(dr);
			// action.moveToElement(ch).build().perform();
			Takescreenshots.screenshots(dr, "Hover Screenshots");
			switch (actionn) {
			case "Rainmodeparams":
				Constant.clickxpath(dr, pro.getProperty("Rainmodeparamss"));
				log.info("Rain params selected");
				break;
			case "ZoneBftimeslots":
				Constant.clickxpath(dr, pro.getProperty("ZoneBftimeslotss"));
				log.info("Zone BF time slots selected");
				System.out.println("Zone operation selected");
				break;
			case "Zonefactorsla":
				Constant.clickxpath(dr, pro.getProperty("Zonefactorslatimes"));
				log.info("Zone factor sla selected");
				break;
			case "Zones":
				Constant.clickxpath(dr, pro.getProperty("Zoning"));
				log.info("Zone selected");
				break;
			}
		} catch (Exception e) {
			log.error("operation not selected", e);

			System.out.println("OPeration not selected");

		}
	}

	public void clickaddzonebtn(String element) {
		Constant.clickxpath(dr, element);
	}

	public void selectcity(String element, String text) {
		Constant.selectbyvistxt_id(dr, element, text);
	}

	public void enterzonenm(String element, String text) {
		Constant.sendkysid(dr, element, text);
	}

	public void enteropentime(String element, String text) {
		Constant.sendkysid(dr, element, text);
	}

	public void enterclosetime(String element, String text) {
		Constant.sendkysid(dr, element, text);
	}

	public void clickisopen(String element) {
		Constant.clickid(dr, element);
	}

	public void enterbannermsg(String element, String data) {
		Constant.sendkysid(dr, element, data);

	}

	public void enterbeefuptime(String element, String data) {
		Constant.sendkysid(dr, element, data);

	}

	public void enterDefaultstoporderfactor(String element, String data) {
		Constant.sendkysid(dr, element, data);

	}

	public void enterDefaultstartorderfactor(String element, String data) {
		Constant.sendkysid(dr, element, data);
	}
	public void enterFloatingsublimit(String element, String data) {
		Constant.sendkysid(dr, element, data);
	}

	public void clickopsoverride(String element) {
		Constant.clickid(dr, element);
	}

	public void enterfloatingcashemail(String element, String data) {
		Constant.sendkysid(dr, element, data);
	}

	public void enterthirdpartyopenmulitplier(String element, String data) {
		Constant.sendkysid(dr, element, data);
	}

	public void enterthirdpartyclosedmulitplier(String element, String data) {
		Constant.sendkysid(dr, element, data);
	}

	public void clickbatchingenabled(String element) {
		Constant.clickid(dr, element);
	}

	public void enternooforderbatch(String element, String data) {
		Constant.sendkysid(dr, element, data);
	}

	public void entermaxslaofbatch(String element, String data) {
		Constant.sendkysid(dr, element, data);
	}

	public void entermaxiteminbatch(String element, String data) {
		Constant.sendkysid(dr, element, data);
	}

	public void enterbatchingversion(String element, String data) {
		Constant.sendkysid(dr, element, data);
	}

	public void entermaxorderdiff(String element, String data) {
		Constant.sendkysid(dr, element, data);
	}

	public void entermaxcustdist(String element, String data) {
		Constant.sendkysid(dr, element, data);
	}

	public void entermaxrestdist(String element, String data) {
		Constant.sendkysid(dr, element, data);
	}

	public void entermaxbatchelapsed(String element, String data) {
		Constant.sendkysid(dr, element, data);
	}

	public void enterthirdpartyratio(String element, String data) {
		Constant.sendkysid(dr, element, data);
	}

	public void clickthirdpartyenabledrainmode(String element) {
		Constant.clickid(dr, element);
	}

	public void clickrainmodeenabled(String element) {
		Constant.clickid(dr, element);
	}

	public void enterorderejectharlimit(String element, String data) {
		Constant.sendkysid(dr, element, data);
	}

	public void entermaxoutofnetworktime(String element, String data) {
		Constant.sendkysid(dr, element, data);
	}

	public void clickcreatebutton(String element) {
		Constant.clickid(dr, element);
	}
	public void enterrainmodestartorder(String element, String data) {
		Constant.sendkysid(dr, element, data);
	}
	public void enterrainmodestoporder(String element, String data) {
		Constant.sendkysid(dr, element, data);
	}
	public void clearall(String element) {
		dr.findElement(By.id(element)).clear();
		;
	}

}


