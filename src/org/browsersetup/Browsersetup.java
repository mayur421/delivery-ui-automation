package org.browsersetup;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class Browsersetup {

	static WebDriver dr;
	static Logger log;
	static Properties pro;
	static String path = "C:\\Users\\mayur.gupta\\Workspace\\Swiggy";
	static FileInputStream fis;

	static String reportLocation = "C:\\Users\\mayur.gupta\\Workspace\\Swiggy\\Log\\Logger.log";
	static String downloadpath = "C:\\Users\\mayur.gupta\\Workspace\\Swiggy\\Downloaded files";

	public static Logger logger1() {
		log = Logger.getLogger(Browsersetup.class);
		return log;
	}

	public static WebDriver setup(String browsertype, String path,
			String loginurl) throws IOException {
		fis = new FileInputStream(path);
		pro = Browsersetup.loadProperties(fis);

		if (browsertype.equalsIgnoreCase("Chrome")) {
			System.setProperty(
					"webdriver.chrome.driver",
					"C:\\Users\\mayur.gupta\\Desktop\\selenium\\chromedriver_win32//chromedriver.exe");
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			capabilities.setCapability(
					CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR,
					UnexpectedAlertBehaviour.IGNORE);
			ChromeOptions options = new ChromeOptions();
			Map<String, Object> prefs = new HashMap<String, Object>();
			options.setExperimentalOption("prefs", prefs);
			options.addArguments("test-type");
			options.addArguments("--always-authorize-plugins=true");
			capabilities
					.setCapability(
							"chrome.binary",
							"C:\\Users\\mayur.gupta\\Desktop\\selenium\\chromedriver_win32\\chromedriver.exe");
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			options.addArguments("start-maximized");
			options.addArguments("enable-npapi");
			options.addArguments("disable-popup-blocking", "true");
			prefs.put("profile.default_content_settings.popups", 0);
			prefs.put("download.default_directory", downloadpath);
			options.setExperimentalOption("prefs", prefs);
			dr = new ChromeDriver(capabilities);
//			log.info("New Browser Instantiated");
//			log.info("Web Application launched successfully");
			dr.get(loginurl);

			// .setPreference("browser.download.dir", "D:\\WebDriverdownloads");

		} else {
			FirefoxProfile profile = new FirefoxProfile();
			profile.setPreference("plugin.default.state", 2);
			dr = new FirefoxDriver(profile);
//			log.info("Firefox Browser launched");
			dr.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
//			log.info("Implicitly waiting for browser to launch successfully");
			dr.manage().window().maximize();
			log.info("Maximizing Browser window");
			dr.get(loginurl);
			log.info("Web Application launched successfully");
		}

		// log.info("setup done");
		return dr;
	}

	public static Properties loadProperties(FileInputStream fis)
			throws IOException {

		pro = new Properties();
		pro.load(fis);
		return pro;
	}

	public static WebDriver getcurrentdriver() {
		if (dr == null)
			return dr = new FirefoxDriver();
		else
			return dr;

	}
}




