package org.TC;


	import java.io.FileInputStream;
	import java.io.IOException;
	import java.sql.Connection;
	import java.sql.SQLException;
	import java.text.DateFormat;
	import java.text.SimpleDateFormat;
	import java.util.Date;
	import java.util.Properties;

	import org.apache.log4j.Logger;
	import org.browsersetup.Browsersetup;
	import org.openqa.selenium.WebDriver;
	import org.testng.annotations.BeforeClass;
	import org.testng.annotations.Test;

	import org.swiggy.pom.GenerateAgreement;
	import org.utility.functions.Constant;
	import org.utility.functions.Login;

	public class Generate_Agreement {
		static WebDriver dr;
		static Properties pro;
		static Logger log;
		static FileInputStream fis;
		static Connection con;
		static String dt;
		static String incentiveid;
		static GenerateAgreement GenerateAgreement;

		@BeforeClass(groups = "swiggy")
		public void logintodelivery() throws IOException, InterruptedException,
				ClassNotFoundException, SQLException {
			fis = new FileInputStream(Constant.objpathgenerateagree);
			System.out.println("Test");
			pro = Browsersetup.loadProperties(fis);
			log = Browsersetup.logger1();
			DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
			Date dateobj = new Date();
			dt = df.format(dateobj);
			dr = Browsersetup.setup("Chrome", Constant.objpathgenerateagree,
					pro.getProperty("loginurl"));
			GenerateAgreement = new GenerateAgreement(dr);
			Login.logintoportal(dr, pro.getProperty("username"),
					pro.getProperty("password"), Constant.objpathgenerateagree);

		}

		@Test(groups = { "swiggy" }, priority = 1)
		public void generate() {
			// Click on Generate Agreement link
			GenerateAgreement.clickgenerateagreelink(pro
					.getProperty("clickgenerateagreementlink"));
			log.info("User clicked on Generate agreement link");
			// Select cities for which agrrement has to be generated
			GenerateAgreement.selectcity(pro.getProperty("Selectcity"),
					pro.getProperty("select_city1"));
			GenerateAgreement.selectcity(pro.getProperty("Selectcity"),
					pro.getProperty("select_city2"));
			log.info("User selected " + pro.getProperty("select_city1") + " and "
					+ pro.getProperty("select_city2") + " for generating agreement");

		}

	}

