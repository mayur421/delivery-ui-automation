package org.TC;


	import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Properties;

	import org.apache.log4j.Logger;
import org.browsersetup.Browsersetup;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.swiggy.pom.ManualSurge;
import org.utility.functions.Constant;
import org.utility.functions.DbConnection;
import org.utility.functions.Extentreportutils;
import org.utility.functions.Login;
import org.utility.functions.Takescreenshots;

import com.relevantcodes.extentreports.LogStatus;

	public class Manual_Surge {
		static ManualSurge ManualSurge;
		static WebDriver dr;
		static Properties pro;
		static Logger log;
		static FileInputStream fis;
		static Connection con;
		static String dt;
		static boolean t;
		static String scrrenpasspath = Constant.scrrenpasspaths;

		@BeforeClass(groups = "swiggy")
		public void logintodelivery() throws IOException, InterruptedException,
				ClassNotFoundException, SQLException {
			fis = new FileInputStream(Constant.objpathmansurge);
			pro = Browsersetup.loadProperties(fis);
			log = Browsersetup.logger1();
			dr = Browsersetup.setup("Chrome", Constant.objpathmansurge,
					pro.getProperty("loginurl"));
			ManualSurge = new ManualSurge(dr);
			Login.logintoportal(dr, pro.getProperty("username"),
					pro.getProperty("password"), Constant.objpathmansurge);

		}

		@Test(groups = { "swiggy" }, priority = 1)
		public void createsurgemanual() throws IOException {
			try {
				// Click Manual Surge link
				log.info("-----------------------------------------Creating manaul surge-------------------------------");
				Extentreportutils.starttest("Create Manual Surge", "To create manual surge");
				HashMap<String, String> map = DbConnection.executeQuery(pro
						.getProperty("query"));
				String id = map.get("id");
				ManualSurge.clickmanualsurge(pro
						.getProperty("Clickmanualsurgelink"));
				log.info("User clicked on manual surge link");
				// Select City for which manual surge has to be created
				ManualSurge.selectcity(pro.getProperty("selectcity"),
						pro.getProperty("select_city"));
				log.info("User selected " + pro.getProperty("select_city")
						+ " for creating manual surge");
				// Select Area for which manual surge has to be created
				ManualSurge.selectarea(pro.getProperty("selectarea"),
						pro.getProperty("select_area"));
				log.info("User selected " + pro.getProperty("select_area")
						+ " for creating manual surge");
				// Enter validity from date/time
				ManualSurge.entervalidityfrom(pro.getProperty("fromdate"),
						pro.getProperty("from_date"), pro.getProperty("from_time"));
				log.info("User entered manaul surge validity from date "
						+ pro.getProperty("from_date") + " and time "
						+ pro.getProperty("from_time"));
				// Enter validity To date/time
				ManualSurge.entervalidityto(pro.getProperty("todate"),
						pro.getProperty("to_date"), pro.getProperty("to_time"));
				log.info("User entered manaul surge validity to date "
						+ pro.getProperty("to_date") + " and time "
						+ pro.getProperty("to_time"));
				// Enter Surge multiplier
				ManualSurge.entersurgemultiplier(
						pro.getProperty("surgemultiplier"),
						pro.getProperty("surge_multiplier"));
				log.info("User enetered surge multiplier as "
						+ pro.getProperty("surge_multiplier"));
				// Click submit button
				ManualSurge.clicksubmitbtn(pro.getProperty("Clicksubmitbutton"));
				// Checking whether manual surge is created or not
				Alert a = dr.switchTo().alert();
				a.accept();
				int x = Integer.parseInt(id) + 1;
				String x1 = String.valueOf(x);
					String query1="Select * from area "
						+ "where name ='"+ pro.getProperty("select_area")+"'";
				HashMap <String,String> map2=DbConnection.executeQuery(query1);
				String areaid=map2.get("id");
				boolean b = ManualSurge.checkmanualsurgecreated(pro.getProperty("query"),areaid,pro.getProperty("surge_multiplier"),
						x1);
				if(b==true)
				{
					log.info("Manual Surge created successfully");
					Extentreportutils.log(LogStatus.PASS, "Create manual surge", " Surge created succesfully",scrrenpasspath+"manualsurgesuccess.png");
					Assert.assertTrue(b);
				}
				else
				{
				log.info("Manual Surge creation failed");
				Extentreportutils.log(LogStatus.FAIL, "Create manual surge", " Surge creation failed",scrrenpasspath+"manualsurgefailed.png");
				Assert.assertTrue(b);
			
					
				}
				} catch (Exception e) {
				log.debug("Not able to create manual surge because of some issue", e);
				Takescreenshots.screenshots(dr, "manualsurgefailed");
				Extentreportutils.log(LogStatus.FAIL, "Create manual surge", " Surge creation failed",scrrenpasspath+"manualsurgefailed.png");
				Assert.fail("Not able to create manual surge because of some issue", e);
			}
		}

	}
