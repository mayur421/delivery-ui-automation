package org.TC;


	import java.io.FileInputStream;
	import java.io.IOException;
	import java.sql.Connection;
	import java.sql.SQLException;
	import java.util.HashMap;
	import java.util.Properties;

	import org.apache.log4j.Logger;
	import org.browsersetup.Browsersetup;
	import org.openqa.selenium.Alert;
	import org.openqa.selenium.WebDriver;
	import org.testng.Assert;
	import org.testng.annotations.AfterClass;
	import org.testng.annotations.BeforeClass;
	import org.testng.annotations.Test;

	import com.relevantcodes.extentreports.LogStatus;

	import org.swiggy.pom.RuleAreaMap;
	import org.swiggy.pom.CreateIncentive_form;
	import org.utility.functions.Constant;
	import org.utility.functions.DbConnection;
	import org.utility.functions.Extentreportutils;
	import org.utility.functions.Login;
	import org.utility.functions.Takescreenshots;

	public class CreateIncentive {
		static CreateIncentive_form CreateIncentive_form;
		static WebDriver dr;
		static Properties pro;
		static Logger log;
		static FileInputStream fis;
		static Connection con;
		static String dt;
		static String incentiveid;
		static RuleAreaMap RuleAreaMap;
		static String x1;
		static String scrrenpasspath = Constant.scrrenpasspaths;

		@BeforeClass(groups = "swiggy")
		public void logintodelivery() throws IOException, InterruptedException,
				ClassNotFoundException, SQLException {
			fis = new FileInputStream(Constant.objpathincentive);
			System.out.println("Test");
			pro = Browsersetup.loadProperties(fis);
			log = Browsersetup.logger1();
			// DateFormat df=new SimpleDateFormat("dd/MM/yy HH:mm:ss");
			// Date dateobj=new Date();
			// dt=df.format(dateobj);
			dr = Browsersetup.setup("Chrome", Constant.objpathincentive,
			pro.getProperty("loginurl"));
			CreateIncentive_form = new CreateIncentive_form(dr);
			RuleAreaMap = new RuleAreaMap(dr);
			Login.logintoportal(dr, pro.getProperty("username"),
			pro.getProperty("password"), Constant.objpathincentive);

		}
	/*	@DataProvider
		public Object[][] Createincnt()
		{
			Object[][] data = new Object[1][1];
			return data;
		}*/

		@Test(groups = { "swiggy" }, priority = 1)
		public void createincentive() throws IOException {

			try {
				log.info("--------------------------------------------Create Incentive Rule--------------------------------");
				Extentreportutils.starttest("Create Incentive Rule",
						"To create incentive rule");
				// Fetching ID of last created incentive rule
				HashMap<String, String> map = DbConnection.executeQuery(pro
						.getProperty("newincentivequery"));
				String rule_id = map.get("id");
				DbConnection.dbclose();
				log.info("Last incentive rule id is " + rule_id);
				System.out.println(rule_id);
				// Click on Create Incentive link
				CreateIncentive_form.selectcreateincetivelink(pro
						.getProperty("clickcreateincentive"));
				log.info("User clicked on create incentive link");
				// Enter Validity from date
				CreateIncentive_form.entervalidityfrom(
						pro.getProperty("validitystartdate"),
						pro.getProperty("Validity_Start_Date"),
						pro.getProperty("Validity_start_time"));
				log.info("User entered validity from date & time as "
						+ pro.getProperty("Validity_Start_Date") + " "
						+ pro.getProperty("Validity_start_time"));
				// Enter Validity to date
				CreateIncentive_form.entervalidityto(
						pro.getProperty("validityenddate"),
						pro.getProperty("Validity_end_Date"),
						pro.getProperty("validity_end_time"));
				log.info("User entered validity to date & time as "
						+ pro.getProperty("Validity_end_Date") + " "
						+ pro.getProperty("validity_end_time"));
				// Enter Incentive code
				CreateIncentive_form.enterincentivecode(
						pro.getProperty("incentivecode"),
						pro.getProperty("Incentive_code"));
				log.info("User entered " + pro.getProperty("Incentive_code")
						+ " as Incentive rule code");
				// Enter Incentive Description
				CreateIncentive_form.enterincentivedescription(
						pro.getProperty("incentivedescription"),
						pro.getProperty("Incentive_description"));
				log.info("User entered " + pro.getProperty("Incentive_description")
						+ " as description");
				// Enter Bonus amount
				CreateIncentive_form.enterbonusamount(
						pro.getProperty("incentivebonus"),
						pro.getProperty("Incentive_bonus"));
				log.info("User entered " + pro.getProperty("Incentive_bonus")
						+ " as incentive bonus");
				// Select Incentive type
				Constant.selectbyvistxt_id(dr, pro.getProperty("incentivetype"),
						pro.getProperty("incentive_typ"));
				log.info("User selected " + pro.getProperty("incentive_typ")
						+ " as incentive type");
				switch (pro.getProperty("incentive_typ")) {
				case "Each":
					// Enter Slot start time
					CreateIncentive_form.enterincentivestarttimeslot(
							pro.getProperty("incentiveslotstarttime"),
							pro.getProperty("Slot_start_time"));
					log.info("User enetered " + pro.getProperty("Slot_start_time")
							+ " as incentive slot start time");
					// Enter Slot end time
					CreateIncentive_form.enterincentiveendtimeslot(
							pro.getProperty("incentiveslotendtime"),
							pro.getProperty("Slot_end_time"));
					log.info("User enetered " + pro.getProperty("Slot_end_time")
							+ " as incentive slot start time");
					// Unselect non-applicable days
					CreateIncentive_form.unselectincentivedays("MONDAY");
					CreateIncentive_form.unselectincentivedays("TUESDAY");
					// Select Each condition
					CreateIncentive_form.selecteachcondition(
							pro.getProperty("Each_condition"),
							pro.getProperty("incentiveeachcondition"));
					log.info("User selected " + pro.getProperty("Each_condition")
							+ " condition");
					break;
				case "Cumulative":
					Constant.selectbyvistxt_id(dr,
							pro.getProperty("incentivetype"),
							pro.getProperty("incentive_typ"));
					log.info("User selected incentive type as "
							+ pro.getProperty("incentive_typ"));
					// Enter slot start time
					CreateIncentive_form.enterincentivestarttimeslot(
							pro.getProperty("incentiveslotstarttime"),
							pro.getProperty("Slot_start_time"));
					log.info("User entered " + pro.getProperty("Slot_start_time")
							+ " as incentive start time slot");
					// Enter Slot end time
					CreateIncentive_form.enterincentiveendtimeslot(
							pro.getProperty("incentiveslotendtime"),
							pro.getProperty("Slot_end_time"));
					log.info("User entered " + pro.getProperty("Slot_end_time")
							+ " as incentive end time slot");
					// Unselect non-applicable days
					CreateIncentive_form.unselectincentivedays("MONDAY");
					CreateIncentive_form.unselectincentivedays("TUESDAY");
					// Select Cumulative time condition
					CreateIncentive_form.selectcumulativetimeconditon(
							pro.getProperty("cumulativedaycondition"),
							pro.getProperty("Time_condition"),
							pro.getProperty("Cumulative_condition"));
					log.info("User entered " + pro.getProperty("Time_condition")
							+ " as cumulative tome condition");
					break;

				case "Minimum Promise":
					Constant.selectbyvistxt_id(dr,
							pro.getProperty("incentivetype"),
							pro.getProperty("incentive_typ"));
					log.info("User selected " + pro.getProperty("incentive_typ")
							+ " as incentive type");
					// Enter incentive start slot
					CreateIncentive_form.enterincentivestarttimeslot(
							pro.getProperty("incentiveslotstarttime"),
							pro.getProperty("Slot_start_time"));
					log.info("User selected " + pro.getProperty("Slot_start_time")
							+ " as incentive slot start time");
					// Enter Slot end time
					CreateIncentive_form.enterincentiveendtimeslot(
							pro.getProperty("incentiveslotendtime"),
							pro.getProperty("Slot_end_time"));
					log.info("User selected " + pro.getProperty("Slot_end_time")
							+ " as incentive slot end time");
					// Unselect non-applicable days
					CreateIncentive_form.unselectincentivedays("MONDAY");
					CreateIncentive_form.unselectincentivedays("TUESDAY");
					// Enter minimum order description minimum orders
					CreateIncentive_form.enterminimumpromiseorders(
							pro.getProperty("Minimumorderpromised"),
							pro.getProperty("minimum_order"));
					log.info("User entered " + pro.getProperty("minimum_order")
							+ " as minimum promised orders");
					// Enter minimum hours logged in
					CreateIncentive_form.enterminimumpromiseloggedtime(
							pro.getProperty("minimumloggedtime"),
							pro.getProperty("Minimum_hours_logged"));
					log.info("User entered "
							+ pro.getProperty("Minimum_hours_logged")
							+ " as minimum promise logged in time");
					break;
				case "Commitment":
					Constant.selectbyvistxt_id(dr,
							pro.getProperty("incentivetype"),
							pro.getProperty("incentive_typ"));
					// Enter number of peaks
					CreateIncentive_form.enternoofpeaks(
							pro.getProperty("noofpeaks"),
							pro.getProperty("no_of_peaks"));
					// Enter slot start time
					CreateIncentive_form.enterincentivestarttimeslot(
							pro.getProperty("incentiveslotstarttime1"),
							pro.getProperty("Slot_start_time"));
					// Enter Slot end time
					CreateIncentive_form.enterincentiveendtimeslot(
							pro.getProperty("incentiveslotendtime1"),
							pro.getProperty("Slot_end_time"));
					// Enter Minimum logged in time
					CreateIncentive_form.enterminmiumloggedintime(
							pro.getProperty("minimumloggedintime"),
							pro.getProperty("commited_Minimum_logged_in_time"));
					// Unselect non-applicable days
					// CreateIncentive_form.unselectincentivedays("MONDAY");
					// CreateIncentive_form.unselectincentivedays("TUESDAY");
					// Enter no of orders
					CreateIncentive_form.entercommtiedsumulativeorders(
							pro.getProperty("ordercommitment"),
							pro.getProperty("commitment_order_value"));
					//

				}
				Takescreenshots.screenshots(dr, "incentivedetails");
				// Click on submit button
				CreateIncentive_form.clicksubmitbutton(pro
						.getProperty("clicksubmitbtn"));
				Thread.sleep(3000);
				int x = Integer.parseInt(rule_id) + 1;
				String x1 = String.valueOf(x);
				// Getting newly created incentive rule id
				HashMap<String, String> map3 = DbConnection.executeQuery(pro
						.getProperty("newincentivequery"));
				String newruleid = map3.get("id");
				DbConnection.dbclose();
				log.info("Expected rule id is " + x1
						+ " and rule id fetched from database is " + newruleid);
				// Checking if newly created incentive id is more than 1 of
				// previously fetched id
				boolean b = x1.equalsIgnoreCase(newruleid);
				if (b == true) {
					Alert a = dr.switchTo().alert();
					a.accept();
					log.info(a.getText());
					Extentreportutils.log(LogStatus.PASS, "Incentive Rule",
							"Incentive rule created successfully", scrrenpasspath
									+ "incentivedetails.png");
					log.info("Incentive rule successfully created");
					Assert.assertTrue(b);
				} else {
					Alert a = dr.switchTo().alert();
					a.accept();
					Extentreportutils.log(LogStatus.FAIL, "Incentive Rule",
							"Incentive rule created successfully", scrrenpasspath
									+ "incentivedetails.png");
					log.info("Incentive creation failed");
					Assert.assertTrue(b, "Incentive rule creation failed");

				}
			} catch (Exception e) {
				System.out.println(e);
				log.info("Some error occured while creating incentive rule", e);
				Extentreportutils.log(LogStatus.FAIL, "Incentive Rule",
						"Incentive rule creation failed", scrrenpasspath
								+ "Incentiverulefailed.png");
				System.out
						.println("Some error occured while creating incentive rule");
			}
			Extentreportutils.endtest();
		}

		@Test(groups = { "swiggy" }, priority = 2, dependsOnMethods = { "createincentive" })
		public void mapincentiverule() throws IOException {
			try {
				log.info("---------------------------------------------------Map Incentive Rule-------------------------------------");
				HashMap<String, String> map = DbConnection.executeQuery(pro
						.getProperty("newincentivequery"));
				incentiveid = (String) map.get("id");
				DbConnection.dbclose();
				Thread.sleep(4000);

				Extentreportutils.starttest("Incetive Rule-area mapping",
						"To map incentive rule to area");
				// Click Rule area map link
				RuleAreaMap.clickruleareamap(pro.getProperty("clickruleareamap"));
				// Enter Rule ID
				RuleAreaMap.enterruleid(pro.getProperty("Enterincentiveid"),
						incentiveid);
				// Click on Submit button
				RuleAreaMap.clicksubmitbtn(pro
						.getProperty("clicksubmitbtnrulearea"));
				// Select city where u want to map incentive
				RuleAreaMap.selectcityformapping(pro.getProperty("selectcity"),
						pro.getProperty("Select_City"));
				// Select area
				RuleAreaMap.selectareaselect(pro.getProperty("areaname"),
						pro.getProperty("area_name"));
				// Select shift
				Thread.sleep(2000);
				RuleAreaMap.selectshiftselect(pro.getProperty("shiftname"),
						pro.getProperty("shigt_name"));
				// Click on save button
				Thread.sleep(2000);
				Takescreenshots.screenshots(dr, "areamapdetails");
				Extentreportutils.log(LogStatus.PASS, "Area map details",
						"Details of area and incentive rule selected",
						scrrenpasspath + "areamapdetails.png");
				RuleAreaMap.clicksavebutton(pro.getProperty("clicksavebtn"));
				Thread.sleep(5000);
				Alert alert = dr.switchTo().alert();
				System.out.println(alert.getText());
				log.info(alert.getText());
				alert.accept();
				String areatm = pro.getProperty("area_name");
				String getareaid = "select * from area where name ='" + areatm
						+ "'";
				HashMap<String, String> map1 = DbConnection.executeQuery(getareaid);
				String area_id = map1.get("id");
				boolean ch = RuleAreaMap.checkruleareamapped(
						pro.getProperty("Ruleareamapquery"), area_id, x1);
				if (ch == true) {
					Takescreenshots.screenshots(dr, "Rulemapped");
					Extentreportutils.log(LogStatus.PASS, "Rule area mapped", x1
							+ " rule id mapped to " + pro.getProperty("area_name")
							+ " area", scrrenpasspath + "Rulemapped.png");
					Assert.assertTrue(ch);
				} else {
					
					Takescreenshots.screenshots(dr, "Ruleareamappedfailed");
					Extentreportutils.log(LogStatus.PASS,
							"Rule area mapped failed", x1 + " rule id mapped to "
									+ pro.getProperty("area_name") + " failed",
							scrrenpasspath + "Ruleareamappedfailed.png");
					Assert.assertTrue(ch,"Rule not mapped to area");

				}
			} catch (Exception e) {
				System.out.println(e);
				
				log.debug("Rule not mapped to area", e);
				System.out.println("Rule not mapped to area");
				Takescreenshots.screenshots(dr, "Ruleareamappedfailed");
				Extentreportutils.log(LogStatus.PASS, "Rule area mapped failed", x1
						+ " rule id mapped to " + pro.getProperty("area_name")
						+ " failed", scrrenpasspath + "Ruleareamappedfailed.png");
				Assert.fail("Rule not mapped to area beacuse of some issue");
			}

		}

		@AfterClass(groups = { "swiggy" })
		public void closebrowser() {
			if (dr != null) {
				System.out.println("Closing the browser");
				dr.quit();
			}
		}

	}