package org.TC;

	import java.io.FileInputStream;
	import java.io.IOException;
	import java.sql.Connection;
	import java.util.Properties;

	import org.swiggy.pom.CandidateSignupForm;
	import org.utility.functions.Constant;
	import org.utility.functions.Login;

	import org.apache.log4j.Logger;
	import org.browsersetup.Browsersetup;
	import org.openqa.selenium.Alert;
	import org.openqa.selenium.By;
	import org.openqa.selenium.JavascriptExecutor;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebElement;
	import org.testng.annotations.AfterClass;
	import org.testng.annotations.BeforeClass;
	import org.testng.annotations.Test;

	import com.relevantcodes.extentreports.ExtentReports;

	public class CandidateiSignUp {
		static CandidateSignupForm CandidateSignupForm;
		static WebDriver dr;
		static Properties pro;
		static String h1;
		static Logger log;
		static FileInputStream fis;
		static Connection con;
		ExtentReports extent;

		@BeforeClass(groups = "swiggy")
		public void logintodelivery() throws IOException, InterruptedException {
			fis = new FileInputStream(Constant.objpath);
			pro = Browsersetup.loadProperties(fis);
			log = Browsersetup.logger1();
			dr = Browsersetup.setup("Chrome", Constant.objpath,
					pro.getProperty("loginurl"));
			CandidateSignupForm = new CandidateSignupForm(dr);
			Login.logintoportal(dr, pro.getProperty("username"),
					pro.getProperty("password"), Constant.objpath);

		}

		@Test(groups = "swiggy", priority = 1,enabled=false)
		public void selectoperation() {
			try {
				log.info("Select operation");
				System.out.println();
				CandidateSignupForm.selectopr(pro.getProperty("operation"));
			} catch (Exception e) {
				System.out.println("Not able to select operation");
			}
		}

		@Test(groups = "swiggy", priority = 2,enabled=false)
		public void addcandidateScreening() {
			try {
				// Click on Add candidate button
				CandidateSignupForm.clickaddcandidatebutton(pro
						.getProperty("Addcandidate"));
				log.info("Click Add candidate Button");
				// Enter Candidate name
				CandidateSignupForm.entername(pro.getProperty("Candidate_nameid"),
						pro.getProperty("DEname"));
				Thread.sleep(2000);
				// Enter Candidate City
				CandidateSignupForm.seletcity(pro.getProperty("Candidate_city"),
						pro.getProperty("City"));
				Thread.sleep(2000);
				// Enter candidate personal contact no
				CandidateSignupForm.enterpersonalctnno(
						pro.getProperty("Candidate_personalno"),
						pro.getProperty("personalno"));
				Thread.sleep(2000);

				// Scrolling down to employment status
				WebElement element = dr.findElement(By.id(pro
						.getProperty("Candidate_emp_status")));
				((JavascriptExecutor) dr).executeScript(
						"arguments[0].scrollIntoView(true);", element);
				Thread.sleep(2000);
				// CandidateSignupForm.selectempstatus(pro.getProperty("Candidate_emp_status"),5);
				CandidateSignupForm.selectempstatus(
						pro.getProperty("Candidate_emp_status"),
						pro.getProperty("Empstatus1"));
				Thread.sleep(10000);
				// Alert alert=dr.switchTo().alert();
				// alert.accept();

			} catch (Exception e) {
				System.out
						.println("Some error occured while adding screening details");
				e.printStackTrace();
				log.info("Some error occured while adding screening details");
				log.error("Error occured while adding screening details", e);
				;
			}

		}

		@Test(groups = "swiggy", priority = 3,enabled=false)
		public void addcandidateonfield() {
			try {

				// JavascriptExecutor js = (JavascriptExecutor)dr;
				// js.executeScript("document.getElementsByID('id_classroom_training_date')[0].removeAttribute('disabled')");
				// js.executeScript("document.getElementsByID('id_classroom_training_date')[0].arguments[0].enabled = true",);
				// ((JavascriptExecutor)dr).executeScript("document.getElementById('id_classroom_training_date').value='02-02-2015'");
				WebElement element6 = dr.findElement(By.id(pro
						.getProperty("Trainingstart")));
				((JavascriptExecutor) dr).executeScript(
						"arguments[0].scrollIntoView(true);", element6);
				CandidateSignupForm.entertrainingstart(
						pro.getProperty("Trainingstart"),
						pro.getProperty("traingstartdate"));
				Thread.sleep(2000);
				CandidateSignupForm.entertrainingend(
						pro.getProperty("Trainingend"),
						pro.getProperty("trainingenddate"));
				Thread.sleep(2000);
				CandidateSignupForm.selectempstatus(
						pro.getProperty("Candidate_emp_status"),
						pro.getProperty("Empstatus2"));
				Thread.sleep(2000);
				Alert alert = dr.switchTo().alert();
				alert.accept();
				Thread.sleep(2000);
			} catch (Exception e) {
				System.out.println("Error occured while entring training details");
				log.info("Error occured while entring training details");
				log.error("Error occured while entring training details", e);
			}
		}

		@Test(groups = "swiggy", priority = 4,enabled=false)
		public void addotherdetails() {
			try {
				WebElement element = dr.findElement(By.id(pro
						.getProperty("curraddress")));
				((JavascriptExecutor) dr).executeScript(
						"arguments[0].scrollIntoView(true);", element);
				Thread.sleep(2000);
				CandidateSignupForm.entercurraddline1(
						pro.getProperty("curraddress"),
						pro.getProperty("addressline1"));
				Thread.sleep(2000);
				CandidateSignupForm.entercurrlandmark(
						pro.getProperty("currlandmark"),
						pro.getProperty("landmark"));
				Thread.sleep(2000);
				CandidateSignupForm.enterpostalcode(pro.getProperty("currzipcode"),
						pro.getProperty("zipcode"));
				Thread.sleep(2000);
				CandidateSignupForm
						.copycurrtoperm(pro.getProperty("copycurrtoper"));
				Thread.sleep(2000);
				CandidateSignupForm.selectdltype(pro.getProperty("DLtype"),
						pro.getProperty("dltyp"));
				Thread.sleep(2000);
				CandidateSignupForm.enterdlid(pro.getProperty("DLid"),
						pro.getProperty("Dlidd"));
				// Scrolling down to DL address element
				WebElement element1 = dr.findElement(By.id(pro
						.getProperty("DLaddress")));
				((JavascriptExecutor) dr).executeScript(
						"arguments[0].scrollIntoView(true);", element1);
				CandidateSignupForm.enterdladd(pro.getProperty("DLaddress"),
						pro.getProperty("DLadd"));
				Thread.sleep(2000);
				CandidateSignupForm.enterdlstate(pro.getProperty("DLstate"),
						pro.getProperty("DLstat"));
				Thread.sleep(2000);
				CandidateSignupForm.enterdlissuedon(pro.getProperty("DLissuedon"),
						pro.getProperty("DLissue"));
				Thread.sleep(2000);
				CandidateSignupForm.enterdlvalidfrm(pro.getProperty("DLvalidfrom"),
						pro.getProperty("DLvalidfr"));
				Thread.sleep(2000);
				CandidateSignupForm.enterdlvalidupto(pro.getProperty("DLvalidup"),
						pro.getProperty("DLvalidupt"));
				Thread.sleep(2000);

				// Scrolling down to RC element
				WebElement element2 = dr
						.findElement(By.id(pro.getProperty("RCnum")));
				((JavascriptExecutor) dr).executeScript(
						"arguments[0].scrollIntoView(true);", element2);
				CandidateSignupForm.enterRcno(pro.getProperty("RCnum"),
						pro.getProperty("RCnumber"));
				Thread.sleep(2000);
				CandidateSignupForm.selectRctyp(pro.getProperty("RCtyp"),
						pro.getProperty("RCtyp1"));
				Thread.sleep(2000);
				CandidateSignupForm.enterPancardno(pro.getProperty("RCpan"),
						pro.getProperty("Pancrdno"));
				Thread.sleep(2000);
				CandidateSignupForm.selectPantyp(pro.getProperty("RCpantyp"),
						pro.getProperty("Pantyp"));
				Thread.sleep(2000);
				CandidateSignupForm.enterBikercno(pro.getProperty("RCbikeregno"),
						pro.getProperty("Bikeregno"));
				Thread.sleep(2000);
				CandidateSignupForm.enterbikename(pro.getProperty("RCBikemodel"),
						pro.getProperty("Bikename"));
				Thread.sleep(2000);

				// Scrolling down to Area and shift details

				WebElement element3 = dr.findElement(By.id(pro
						.getProperty("Areareportarea")));
				((JavascriptExecutor) dr).executeScript(
						"arguments[0].scrollIntoView(true);", element3);
				CandidateSignupForm.selectreportarea(
						pro.getProperty("Areareportarea"),
						pro.getProperty("reportingareea"));
				Thread.sleep(2000);
				CandidateSignupForm.selectsecondaryarea(
						pro.getProperty("secondaryarea"),
						pro.getProperty("secondaryarea1"));
				Thread.sleep(2000);
				CandidateSignupForm.selectshifttyp(pro.getProperty("Shifttyp"),
						pro.getProperty("shifttyp1"));
				Thread.sleep(2000);
				CandidateSignupForm.selectfullshifttyp(pro
						.getProperty("fulltimeshift"));
				Thread.sleep(2000);

				// scrolling down to bank details

				WebElement element4 = dr.findElement(By.id(pro
						.getProperty("bankname")));
				((JavascriptExecutor) dr).executeScript(
						"arguments[0].scrollIntoView(true);", element4);
				Thread.sleep(2000);
				CandidateSignupForm.enteruserbankname(pro.getProperty("bankname"),
						pro.getProperty("Bankusername"));
				Thread.sleep(2000);
				CandidateSignupForm.enterbankaccountno(
						pro.getProperty("bankacctno"),
						pro.getProperty("bankaccountno"));
				Thread.sleep(2000);
				CandidateSignupForm.enterbankname(
						pro.getProperty("namebackaccount"),
						pro.getProperty("bankaccountname"));
				Thread.sleep(2000);
				CandidateSignupForm.enterIfsccode(pro.getProperty("bankifsc"),
						pro.getProperty("IFSccode"));
				Thread.sleep(2000);

				// Scrolling down to other details
				WebElement element5 = dr.findElement(By.id(pro
						.getProperty("Security_deposit")));
				((JavascriptExecutor) dr).executeScript(
						"arguments[0].scrollIntoView(true);", element5);
				Thread.sleep(2000);
				CandidateSignupForm.entersecuirtydeposits(
						pro.getProperty("Security_deposit"),
						pro.getProperty("Securitydeposit"));
				Thread.sleep(2000);
				CandidateSignupForm.enterseiggynumber(
						pro.getProperty("swiggymobileno"),
						pro.getProperty("swiggyno"));
				Thread.sleep(2000);

				// Scrolling down to Employment status

				WebElement element6 = dr.findElement(By.id(pro
						.getProperty("Candidate_emp_status")));
				((JavascriptExecutor) dr).executeScript(
						"arguments[0].scrollIntoView(true);", element6);
				CandidateSignupForm.selectempstatus(
						pro.getProperty("Candidate_emp_status"),
						pro.getProperty("Empstatus3"));
				Thread.sleep(2000);
				Alert alert = dr.switchTo().alert();
				alert.accept();
				Thread.sleep(2000);
				Alert alert1 = dr.switchTo().alert();
				alert1.accept();
				Thread.sleep(2000);

				// Click on save button
				CandidateSignupForm.clicksavebutton(pro
						.getProperty("clicksavebutton"));

			} catch (Exception e) {
				System.out.println("Employee not confirmed working with swiggy");
				log.info("Employee not confirmed working with swiggy");
				log.error("Employee not confirmed working with swiggy", e);
			}
		}

		
		
		
		
		@AfterClass(groups = "swiggy")
		public void closebrowser() {
			if (dr != null) {
				System.out.println("Closing the browser");
				dr.quit();
			}
		}

	}