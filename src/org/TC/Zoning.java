package org.TC;


	import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;

	import org.apache.log4j.Logger;
import org.browsersetup.Browsersetup;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.swiggy.pom.Addzone_form;
import org.swiggy.pom.BfTimeSlots;
import org.swiggy.pom.RainModeParams;
import org.utility.functions.Constant;
import org.utility.functions.DbConnection;
import org.utility.functions.Extentreportutils;
import org.utility.functions.Login;
import org.utility.functions.Takescreenshots;

import com.relevantcodes.extentreports.LogStatus;

	public class Zoning {
		static Addzone_form Addzone_form;
		static RainModeParams RainModeParams;
		static BfTimeSlots BfTimeSlots;
		static WebDriver dr;
		static Properties pro;
		static String h1;
		static Logger log;
		static FileInputStream fis;
		static Connection con;
		static String dt;
		static String scrrenpasspath = Constant.scrrenpasspaths;

		@BeforeClass(groups = "swiggy")
		public void logintodelivery() throws IOException, InterruptedException,
				ClassNotFoundException, SQLException {
			fis = new FileInputStream(Constant.objpathzone);
			pro = Browsersetup.loadProperties(fis);
			log = Browsersetup.logger1();
			DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
			Date dateobj = new Date();
			dt = df.format(dateobj);
			dr = Browsersetup.setup("Chrome", Constant.objpathzone,
					pro.getProperty("loginurl"));
			Addzone_form = new Addzone_form(dr);
			Login.logintoportal(dr, pro.getProperty("username"),
					pro.getProperty("password"), Constant.objpathzone);

		}

		@Test(groups = "swiggy", priority = 1)
		public void zonesadd() throws IOException {
			// Select Zone Operation
			try {
				Extentreportutils.starttest("Add Zone", "To add a Zone");
				log.info("Select zone operation");
				Addzone_form.selectzoneopr(pro.getProperty("operation"));
				System.out.println("Zone operation selected");
			} catch (Exception e) {
				System.out.println("Not able to select zone operation");
			}

			try {
				// Click on Add Zone button
				Addzone_form.clickaddzonebtn(pro.getProperty("Addzonebtn"));
				// Select City
				Thread.sleep(2000);
				log.info("User Clicked on Add Zone button");
				Addzone_form.selectcity(pro.getProperty("selectcity"),
						pro.getProperty("select_city"));
				Thread.sleep(2000);
				log.info("User selected " + pro.getProperty("select_city")
						+ " city");
				// Enter Zone name
				Addzone_form.enterzonenm(pro.getProperty("Enterzonename"),
						pro.getProperty("zone_name") + dt);
				log.info("User entered " + pro.getProperty("zone_name")
						+ " as Zone Name");
				// Enter Zone open time
				Addzone_form.enteropentime(pro.getProperty("Opentm"),
						pro.getProperty("open_time"));
				log.info("User entered " + pro.getProperty("open_time")
						+ " as zone open time");
				// Enter Zone close time
				Addzone_form.enterclosetime(pro.getProperty("Closetm"),
						pro.getProperty("close_time"));
				log.info("User enetered " + pro.getProperty("close_time")
						+ " as zone close time");
				// Select is open/close

				// Addzone_form.clickisopen(pro.getProperty("is_openn"));

				// Enter Banner Message
				Addzone_form.enterbannermsg(pro.getProperty("bannermessage"),
						pro.getProperty("banner_message"));
				log.info("User entered " + pro.getProperty("banner_message")
						+ " as banner message");
				Takescreenshots.screenshots(dr, "createzone1");
				Extentreportutils.log(LogStatus.PASS, "Enter details",
						"To enter details", scrrenpasspath + "createzone1.png");
				// Scrolling down to enter values in other fields
				WebElement element = dr.findElement(By.id(pro
						.getProperty("beefuptime")));
				((JavascriptExecutor) dr).executeScript(
						"arguments[0].scrollIntoView(true);", element);

				// Clear default beef up time
				Addzone_form.clearall(pro.getProperty("beefuptime"));
				// Enter beef up time
				Addzone_form.enterbeefuptime(pro.getProperty("beefuptime"),
						pro.getProperty("beefup_time"));
				log.info("User entered " + pro.getProperty("beefup_time")
						+ " as beef up time");
				// Clear default stop order factor
				Addzone_form.clearall(pro.getProperty("Defaultstoporder"));
				// Enter Default stop order factor
				Addzone_form.enterDefaultstoporderfactor(
						pro.getProperty("Defaultstoporder"),
						pro.getProperty("stop_order_factor"));
				log.info("User entered " + pro.getProperty("stop_order_factor")
						+ " as default stop order factor");
				// Clear default Start order factor
				Addzone_form.clearall(pro.getProperty("Defaultstartorder"));
				// Enter default start order factor
				Addzone_form.enterDefaultstartorderfactor(
						pro.getProperty("Defaultstartorder"),
						pro.getProperty("start_order_factor"));
				log.info("User entered " + pro.getProperty("start_order_factor")
						+ " as default start order factor");
				// Clear default Floating cash sub-limit
				Addzone_form.clearall(pro.getProperty("floatingcashsublimit"));
				// Enter default floating cash sublimit
				Addzone_form.enterDefaultstartorderfactor(
						pro.getProperty("floatingcashsublimit"),
						pro.getProperty("floating_cash_sublimit"));
				log.info("User entered " + pro.getProperty("floating_cash_sublimit")
						+ " as default floating cash sublimit");
				
				
				// Click OPS override checkbox
				// Addzone_form.clickopsoverride(pro.getProperty("ops_override"));

				// Enter floating cash email check
				Addzone_form.enterfloatingcashemail(
						pro.getProperty("floatingcashemilcheck"),
						pro.getProperty("floating_cash"));
				log.info("User entered " + pro.getProperty("floating_cash")
						+ " as floating cash");
				// Clear Third party open multiplier
				Addzone_form.clearall(pro.getProperty("thirdpartyopnmultiplier"));
				// Enter Third party open multiplier
				Addzone_form.enterthirdpartyopenmulitplier(
						pro.getProperty("thirdpartyopnmultiplier"),
						pro.getProperty("third_party_multiplier"));
				log.info("User entered "
						+ pro.getProperty("third_party_multiplier")
						+ " as third party multiplier");
				// Clear third party close multiplier
				Addzone_form.clearall(pro.getProperty("thirdpartyclsmultiplier"));
				// Enter Third party close multiplier
				Addzone_form.enterthirdpartyclosedmulitplier(
						pro.getProperty("thirdpartyclsmultiplier"),
						pro.getProperty("third_party_cls_multi"));
				log.info("User entered " + pro.getProperty("third_party_cls_multi")
						+ " as third party close multiplier");
				// To check enable batching checkbox
				Addzone_form.clickbatchingenabled(pro
						.getProperty("Batchingenabledcheck"));
				log.info("User selected batching enable checkbox");
				// Clear default not of order to batch
				Addzone_form.clearall(pro.getProperty("maxorderinbatch"));
				// Enter maximum orders to batch
				Addzone_form.enternooforderbatch(
						pro.getProperty("maxorderinbatch"),
						pro.getProperty("max_orders_in_batch"));
				log.info("User entered " + pro.getProperty("max_orders_in_batch")
						+ " as maximum order a batch can have");
				// Clear Default max SLA of batch
				Addzone_form.clearall(pro.getProperty("maxslaofbatch"));
				// Enter max SLA of batch
				Addzone_form.entermaxslaofbatch(pro.getProperty("maxslaofbatch"),
						pro.getProperty("max_sla_of_batch"));
				log.info("User entered " + pro.getProperty("max_sla_of_batch")
						+ " as max SLA limit for batching");
				Takescreenshots.screenshots(dr, "createzone2");
				Extentreportutils.log(LogStatus.PASS, "Enter other details",
						"To enter other details", scrrenpasspath
								+ "createzone2.png");
				// clear default max item in batch
				Addzone_form.clearall(pro.getProperty("maxiteminbatch"));
				// Enter Max item in batch
				Addzone_form.entermaxiteminbatch(pro.getProperty("maxiteminbatch"),
						pro.getProperty("max_item_in_batch"));
				log.info("User entered " + pro.getProperty("max_item_in_batch")
						+ " as max item limit for batching");
				// Clear default batching version
				Addzone_form.clearall(pro.getProperty("batchingversion"));
				// Enter Batching version
				Addzone_form.enterbatchingversion(
						pro.getProperty("batchingversion"),
						pro.getProperty("Batching_version"));
				log.info("User entered " + pro.getProperty("Batching_version")
						+ " as batching version value");
				// Clear Default max orders in batch
				Addzone_form.clearall(pro.getProperty("maxxorderdiff"));
				// Enter max orders difference in Batch
				Addzone_form.entermaxorderdiff(pro.getProperty("maxxorderdiff"),
						pro.getProperty("max_diff_in_orders"));
				log.info("User entered " + pro.getProperty("max_diff_in_orders")
						+ " as maximum difference in order");
				// Clear default max customer distance
				Addzone_form.clearall(pro.getProperty("maxcustomerdist"));
				// Enter Max customer difference
				Addzone_form.entermaxcustdist(pro.getProperty("maxcustomerdist"),
						pro.getProperty("max_customer_Dist"));
				log.info("User entered " + pro.getProperty("max_customer_Dist")
						+ " as maximum customer limit for batching");
				// Clear default max restaurant difference
				Addzone_form.clearall(pro.getProperty("maxrestdist"));
				// Enter Max restaurant Difference
				Addzone_form.entermaxrestdist(pro.getProperty("maxrestdist"),
						pro.getProperty("max_rest_dist"));
				log.info("User entered " + pro.getProperty("max_rest_dist")
						+ " as maximum restaurant distance for batching");
				// Clear default max elapsed time
				Addzone_form.clearall(pro.getProperty("maxelapsedtime"));
				// Enter Max Elapsed time
				Addzone_form.entermaxbatchelapsed(
						pro.getProperty("maxelapsedtime"),
						pro.getProperty("max_elaspedtime"));
				log.info("User entered " + pro.getProperty("max_elaspedtime")
						+ " as maximum elapsed time for batching");
				// Enter Third part ratio
				Addzone_form.enterthirdpartyratio(
						pro.getProperty("thordpartratio"),
						pro.getProperty("Thrid_paerty_ratio"));
				log.info("User entered " + pro.getProperty("Thrid_paerty_ratio")
						+ " as third party ratio");
				// Select is third party rain enabled
				// Addzone_form.clickthirdpartyenabledrainmode(pro.getProperty("Thirdpartisenablerain"));
				// Select is rain enabled
				Addzone_form
						.clickrainmodeenabled(pro.getProperty("enablerainmode"));
				log.info("User enabled rain mode");
				// Clear default order reject hard limit
				Addzone_form.clearall(pro.getProperty("Orderreecthardlimit"));
				// Enter order reject hard limit
				Addzone_form.enterorderejectharlimit(
						pro.getProperty("Orderreecthardlimit"),
						pro.getProperty("Order_reject_hard_limit"));
				log.info("User entered "
						+ pro.getProperty("Order_reject_hard_limit")
						+ " as order reject hard limit");
				// Clear default maximum out of network time
				Addzone_form.clearall(pro.getProperty("Maxoutofnetworktime"));
				// Enter maximum out of network time
				Addzone_form.entermaxoutofnetworktime(
						pro.getProperty("Maxoutofnetworktime"),
						pro.getProperty("maximum_out_network_time"));
				log.info("User entered "
						+ pro.getProperty("maximum_out_network_time")
						+ " as maximum out of network time");
				// Clear default rain mode stop order factor
				Addzone_form.clearall(pro.getProperty("rainmodestoporder"));
				// Enter rain mode stop order factor
				Addzone_form.enterrainmodestoporder(
						pro.getProperty("rainmodestoporder"),
						pro.getProperty("rain_mode_stop_order"));
				log.info("User entered " + pro.getProperty("rain_mode_stop_order")
						+ " as rain mode stop order factor");

				// Clear default rain mode start order factor
				Addzone_form.clearall(pro.getProperty("rainmodestartorder"));
				// Enter rain mode start order factor
				Addzone_form.enterrainmodestoporder(
						pro.getProperty("rainmodestartorder"),
						pro.getProperty("rain_mode_start_order"));
				log.info("User entered " + pro.getProperty("rain_mode_start_order")
						+ " as rain mode start order factor");
				Takescreenshots.screenshots(dr, "createzone3");
				Extentreportutils.log(LogStatus.PASS, "Enter other details",
						"To enter other details", scrrenpasspath
								+ "createzone3.png");
				// Click on Create button
				Addzone_form.clickcreatebutton(pro.getProperty("createbutton"));
				Thread.sleep(5000);
				String msg = dr.findElement(
						By.xpath(pro.getProperty("Successmessagexpath"))).getText();
				boolean b = msg.equalsIgnoreCase(pro
						.getProperty("Zoneaddsuccessmessage"));
				if (b == true) {

					log.info("User successfully added new Zone");
					Takescreenshots.screenshots(dr, "Zonesuccesfullycreated");
					Extentreportutils.log(LogStatus.PASS, "Click create button",
							"To click on create zone button", scrrenpasspath
									+ "Zonesuccesfullycreated.png");
					Extentreportutils.endtest();
					Assert.assertTrue(b);
				} else {
					log.info("New zone creation failed");
					Takescreenshots.screenshots(dr, "Zonecreationfail");
					Extentreportutils.log(LogStatus.FAIL, "Click create button",
							"To click on create zone button", scrrenpasspath
									+ "Zonecreationfail.png");
					Extentreportutils.endtest();
					Assert.assertTrue(b);

				}

			} catch (Exception e) {
				System.out.println("User not able to Add Zone");
				log.debug("User not able to Add zone", e);
				Takescreenshots.screenshots(dr, "Zonecreationfail");
				Extentreportutils.log(LogStatus.FAIL, "Click create button",
						"To click on create zone button", scrrenpasspath
								+ "Zonecreationfail.png");
				Extentreportutils.endtest();
				Assert.fail("New zone creation failed", e);

			}
		}

		@Test(groups = "swiggy", priority = 2,enabled=false)
		void addrainparameters() throws IOException {
			// Select Zone Operation
			try {
				RainModeParams = new RainModeParams(dr);
				log.debug("Select zone operation");
				Extentreportutils.starttest("Add rain mode params", "To add rain mode params");
				Addzone_form.selectzoneopr(pro.getProperty("operation1"));
				System.out.println("Rain mode params operation selected");
			} catch (Exception e) {
				System.out.println("Not able to select Rain mode params operation");
			}
			try {
				// Click Add Rain mode param button
				RainModeParams.clickaddrainmodeparam(pro
						.getProperty("Addrainparamxpath"));
				Thread.sleep(2000);
				log.info("Rain mode param add button clicked");
				// Select Rain mode param City
				RainModeParams.selectcity(pro.getProperty("Rainmodecity"),
						pro.getProperty("Rain_mode_city"));
				log.info("City for rain mode param is selected as "
						+ pro.getProperty("Rain_mode_city"));

				// Clear default surge fee value
				RainModeParams.clearall(pro.getProperty("Rainmodesurgefee"));
				// Enter Rain mode param surge fee
				RainModeParams.entersurgefee(pro.getProperty("Rainmodesurgefee"),
						pro.getProperty("Rain_mode_Surge_fee"));
				log.info("Rain mode surge fee appilicable is "
						+ pro.getProperty("Rain_mode_Surge_fee"));

				// Clear default Rain mode Banner Factor
				RainModeParams.clearall(pro.getProperty("RainmodeBM"));
				// Enter Rain param banner message
				RainModeParams.enterbannermessage(pro.getProperty("RainmodeBM"),
						pro.getProperty("Rain_mode_BM"));
				log.info("Rain mode Banner messsage is "
						+ pro.getProperty("Rain_mode_BM"));

				// Clear default last mile cap distance
				RainModeParams.clearall(pro.getProperty("lastmilecap"));
				// Enter rain mode param last mile cap distance
				RainModeParams.enterlastmilcap(
						pro.getProperty("lastmilecap"),
						pro.getProperty("last_mile_cap"));
				log.info("Rain mode last mile cap is set as "
						+ pro.getProperty("last_mile_cap"));

				// Clear Default max SLA limit
				RainModeParams.clearall(pro.getProperty("maxsla"));
				// Enter max SLA limit
				RainModeParams.entermaxsla(
						pro.getProperty("maxsla"),
						pro.getProperty("max_sla"));
				log.info("Rain mode max SLA limit is set as "
						+ pro.getProperty("max_sla"));

				// Clear Default Beef up time
				RainModeParams.clearall(pro.getProperty("RainmodeBeeftime"));
				// Enter beef up time
				RainModeParams.enterbeefuptime(pro.getProperty("RainmodeBeeftime"),
						pro.getProperty("Rain_mode_beef_time"));
				log.info("Rain mode beef up time is set as "
						+ pro.getProperty("Rain_mode_beef_time"));

				// Clear default Surge multiplier for DE
				RainModeParams.clearall(pro.getProperty("Rainmodesurgemultiplier"));
				// Enter Surge multiplier for DE
				RainModeParams.entersurgemultilpier(
						pro.getProperty("Rainmodesurgemultiplier"),
						pro.getProperty("Rain_mode_surge_multiplier"));
				log.info("Rain mode surge multiplier is set as "
						+ pro.getProperty("Rain_mode_surge_multiplier"));

				// Clear default surge applicable for min
				RainModeParams.clearall(pro.getProperty("Rainmodesurgeapplicabke"));
				// Enter surge applicable for min
				RainModeParams.entersurgemin(
						pro.getProperty("Rainmodesurgeapplicabke"),
						pro.getProperty("Rain_mode_surge_time"));
				log.info("Rain mode surge appilcable upto "
						+ pro.getProperty("Rain_mode_surge_time") + " mins");

				// Click Save button
				RainModeParams.clicksavebutton(pro.getProperty("savebutton"));
				log.info("User clicked on save button");
				String msg=dr.findElement(By.xpath(pro.getProperty("newaddsuccessmsg"))).getText();
				boolean b=msg.contains("added successfully");
				if(b==true)
				{
					log.info("Rain mode param created for zone");
					Takescreenshots.screenshots(dr, "zonerainmd");
					Extentreportutils
							.log(LogStatus.PASS, "Click create button",
									"Click create button to create rain mode params for zone",
											scrrenpasspath+"zonerainmd.png");
					Extentreportutils.endtest();
					Assert.assertTrue(b);
				} else {
					log.info("Rain mode creation failed for Zone");
					Takescreenshots.screenshots(dr, "zonerainmodeparamfail");
					Extentreportutils
							.log(LogStatus.FAIL, "Click create button",
									"Click create button to create rain mode params for zone ",scrrenpasspath
											+ "zonerainmodeparamfail.png");
					Extentreportutils.endtest();
					Assert.fail();
				}
				
			} catch (Exception e) {
				System.out.println("Not able to add Rain mode params");
				Takescreenshots.screenshots(dr, "zonerainmodeparamfail");
				Extentreportutils
						.log(LogStatus.FAIL, "Click create button",
								"Click create button to create rain mode params for zone ",scrrenpasspath
										+ "zonerainmodeparamfail.png");
				Extentreportutils.endtest();
				Assert.fail();
			
			}

		}

		@Test(groups = "swiggy", priority = 3,enabled=false)
		void addbftimeslots() throws IOException {
			// Select Zoning Operation
			try {
				Extentreportutils.starttest("Add BF time slots",
						"To add BF time slots for zone");
				BfTimeSlots = new BfTimeSlots(dr);
				log.debug("Select zone operation");
				Addzone_form.selectzoneopr(pro.getProperty("operation2"));
				System.out.println("Zone bf Time slots selected");
			} catch (Exception e) {
				System.out.println("Not able to select bf time slots operation");
				log.debug("Not able to select bf time slots operation", e);
			}

			try {
				HashMap<String, String> map = DbConnection.executeQuery(pro
						.getProperty("getnewlyaddedzonequery"));
				String zon = (String) map.get("name");
				log.info("User will create BF time slots for " + zon + " zone");
				// Click Add bf time slots button
				BfTimeSlots.clickAddbftimeslotbutton(pro
						.getProperty("Addbftimeslotbutton"));
				log.info("User clicked on add bf time slots button");
				// Select BF Time slot Zone
				BfTimeSlots.selectbftimslotzone(pro.getProperty("bftimeslotszone"),
						zon);
				log.info("User selected " + zon + " zone");
				// Enter BF time slot open time
				BfTimeSlots.enterbftimeslotopentime(
						pro.getProperty("bftimeslotsopentime"),
						pro.getProperty("bf_time_slot_open_time"));
				log.info("User entered "
						+ pro.getProperty("bf_time_slot_open_time")
						+ " as bf time slot open time");
				// Enter BF time slot close time
				BfTimeSlots.enterbftimeslotsclosetime(
						pro.getProperty("bftimeslotclosetime"),
						pro.getProperty("bf_time_Slot_close_time"));
				log.info("User selected "
						+ pro.getProperty("bf_time_Slot_close_time")
						+ " as bf time slot close time");
				// Clear BF time slot start order factor
				BfTimeSlots.clearall(pro.getProperty("bftimeslotstartorderfactor"));
				// Enter BF time slot start order factor
				BfTimeSlots.enterbftimeslotstartorderfactor(
						pro.getProperty("bftimeslotstartorderfactor"),
						pro.getProperty("bf_time_slot_start_order"));
				log.info("User entered "
						+ pro.getProperty("bf_time_slot_start_order")
						+ " as bf time slot start order factor");
				// Clear BF time slot stop order factor
				BfTimeSlots.clearall(pro.getProperty("bftimeslotstoporderfactor"));
				// Enter BF time slot stop order factor
				BfTimeSlots.enterbftimeslotstoporderfactor(
						pro.getProperty("bftimeslotstoporderfactor"),
						pro.getProperty("bf_time_slot_stop_order"));
				log.info("User entered "
						+ pro.getProperty("bf_time_slot_stop_order")
						+ " as bf time slot stop order factor");
				Takescreenshots.screenshots(dr, "enterbftimeslotdetail");
				Extentreportutils.log(LogStatus.PASS, "Entered details",
						"Details entered by users", scrrenpasspath
								+ "enterbftimeslotdetail.png");
				// Click save button
				BfTimeSlots.clicksavebtn(pro.getProperty("bftimeslotclicksavebtn"));
				String msg = dr.findElement(
						By.xpath(pro.getProperty("Bftimeslotsuccessmessage")))
						.getText();
				boolean b = msg.contains("added successfully");
				if (b == true) {
					log.info("Bf time slot successfully created for " + zon
							+ " zone");
					Takescreenshots.screenshots(dr, "zonebftimeslot");
					Extentreportutils
							.log(LogStatus.PASS, "Click create button",
									"Click create button to create bf time slot for "
											+ zon, scrrenpasspath
											+ "zonebftimeslot.png");
					Extentreportutils.endtest();
					Assert.assertTrue(b);
				} else {
					log.info("Bf time slot creation failed for " + zon + " Zone");
					Takescreenshots.screenshots(dr, "zonebftimeslotfail");
					Extentreportutils
							.log(LogStatus.FAIL, "Click create button",
									"Click create button to create bf time slot for "
											+ zon, scrrenpasspath
											+ "zonebftimeslotfail.png");
					Extentreportutils.endtest();
					Assert.fail();
				}
			} catch (Exception e) {
				System.out.println("Not able to Add zone BF time slot");
				log.debug("Not able to Add zone BF time slot", e);
				Takescreenshots.screenshots(dr, "zonebftimeslotfail");
				Extentreportutils.log(LogStatus.FAIL, "Click create button",
						"Click create button to create bf time slot",
						scrrenpasspath + "zonebftimeslotfail.png");
				Extentreportutils.endtest();
				Assert.fail();
			}
		}

		@AfterClass(groups = "swiggy",enabled=false)
		public void closebrowser() {
			if (dr != null) {
				System.out.println("Closing the browser");
				dr.quit();
			}
		}
	}