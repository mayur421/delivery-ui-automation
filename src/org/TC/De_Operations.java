package org.TC;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Properties;

import org.swiggy.pom.DeOperations;
import org.apache.log4j.Logger;
import org.browsersetup.Browsersetup;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.utility.functions.Constant;
import org.utility.functions.DbConnection;
import org.utility.functions.Extentreportutils;
import org.utility.functions.Login;
import org.utility.functions.Takescreenshots;

import com.relevantcodes.extentreports.LogStatus;

public class De_Operations {
	
	static DeOperations DeOperations;
	static WebDriver dr;
	static Properties pro;
	static Logger log;
	static FileInputStream fis;
	static Connection con;
	static String x1;
	static String scrrenpasspath = Constant.scrrenpasspaths;

	@BeforeClass(groups = "swiggy")
	public void logintodelivery() throws IOException, InterruptedException,
			ClassNotFoundException, SQLException {
		fis = new FileInputStream(Constant.objpathoperations);
		pro = Browsersetup.loadProperties(fis);
		log = Browsersetup.logger1();
		// DateFormat df=new SimpleDateFormat("dd/MM/yy HH:mm:ss");
		// Date dateobj=new Date();
		// dt=df.format(dateobj);
		dr = Browsersetup.setup("Chrome", Constant.objpathoperations,
		pro.getProperty("loginurl"));
		DeOperations = new DeOperations(dr);
		Login.logintoportal(dr, pro.getProperty("username"),
		pro.getProperty("password"), Constant.objpathoperations);

	}
	
	
	
	@Test(groups = { "swiggy" }, priority = 1)
	public void autoassignsearchcheck() throws IOException
	{
		try{
		Extentreportutils.starttest("Auto-Assign logs", "To check auto aassing logs search");
		//Selecting auto-assign operation
		DeOperations.clickoperation(pro.getProperty("auto_assign_select_operation"));
		log.info("User selected auto-assignment logs operation");
		//Select zone for checking search
		DeOperations.autoassignselectzone(pro.getProperty("autoassignselectzone"), pro.getProperty("auto_assign_select_zone"));
		log.info("User selected "+pro.getProperty("auto_assign_select_zone")+" as zone");
		//Select parameter for checking search
		DeOperations.autoassignselectparameter(pro.getProperty("autoassignselectparameter"), pro.getProperty("auto_assign_select_parameter"));
		log.info("User selected "+pro.getProperty("auto_assign_select_parameter")+" as parameter");
		//Click on search button
		DeOperations.clicksearchbtn(pro.getProperty("autoassignclicksearchbtn"));
		log.info("User clicked on search button");
		//Getting no of rows displayed
				
		String query="select *,count(*) from auto_assign_audit aa "
		+ " inner join auto_assignment_params ap on aa.parameter_id=ap.id"
		+" inner join zone z on aa.zone_id=z.id"
		+" where ap.parameter= '" +pro.getProperty("auto_assign_select_parameter")+"'"
		+" and z.name= '" +pro.getProperty("auto_assign_select_zone")+"'";
		HashMap<String,String> map=DbConnection.executeQuery(query);
		String s=map.get("count(*)");
		log.info("Count from DB is "+s);
		int x=DeOperations.autoassigngetrowcount(pro.getProperty("autoassignrowcountaftsrch"));
		log.info("Search result count after applying filters is "+x);
		int x1=Integer.parseInt(s);
		boolean ch=(x==x1);
		if(ch==true)
		{
			log.info("Search results are correct");
			Takescreenshots.screenshots(dr,"autoassignsearchresult");
			Extentreportutils.log(LogStatus.PASS, "To verify search results", "To verify correct record appearing after applying filters", scrrenpasspath+"autoassignsearchresult.png");
			Assert.assertTrue(ch);
		}
		else
		{
			log.info("Search results are incorrect");
			Takescreenshots.screenshots(dr,"autoassignsearchresultincorrect");
			Extentreportutils.log(LogStatus.FAIL, "To verify search results", "To verify correct record appearing after applying filters", scrrenpasspath+"autoassignsearchresultincorrect.png");
			Assert.assertTrue(ch);
		}
		}
		catch(Exception e)
		{
			log.debug("User not able to search because of some issue", e);
			Takescreenshots.screenshots(dr, "autoassignsearchfail");
			Extentreportutils.log(LogStatus.FAIL, "To verify search results", "To verify correct record appearing after applying filters", scrrenpasspath+"autoassignsearchfail.png");
			Assert.fail("User not able to search beacuse of some issue", e);
		}
	}
	
	
	
	@Test(priority=2,groups= {"swiggy"})
	public void clickautoassingentry()
	{
		try
		{
			Extentreportutils.starttest("Click Auto-Assign logs entry", "To click auto aassing logs entry and retrieve details");
			DeOperations.clickDeincentivelogentry(pro.getProperty("autoassignclickentry"));
			new WebDriverWait(dr,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(pro.getProperty("autoassignpresenceofelementid"))));
			Takescreenshots.screenshots(dr, "autoassigndetaisl");
			HashMap<String,String> map=DeOperations.getautoassigndetails(pro.getProperty("autoassignentrydetailsfields"), pro.getProperty("autoassignentrydetailsvalues"));
			log.info("Auto Assign deatils for id "+map.get("Id")+" are as follows");
			log.info("ID "+map.get("ID"));
			log.info("Paramter is "+map.get("Parameter"));
			log.info("Zone is "+map.get("Zone"));
			log.info("Values is "+map.get("Value"));
			log.info("User is "+map.get("User"));
			log.info("TimeStamp is "+map.get("TimeStamp"));
			boolean b=map.get("Parameter")!=null;
			if(b==true)
			{
				log.info("User able to get auto assign details");
				Extentreportutils.log(LogStatus.PASS, "Get auto-assign logs details", "To get detailed auto-assign logs", scrrenpasspath+"autoassigndetaisl.png");
				Assert.assertTrue(b);
			}
			else
			{
				log.info("User able to get auto assign details");
				Extentreportutils.log(LogStatus.FAIL, "Get auto-assign logs details", "To get detailed auto-assign logs", scrrenpasspath+"autoassigndetaisl.png");
				Assert.assertTrue(b);
			}
			
		}
		catch(Exception e)
		{
			log.info("Not able to get auto assign details beacuse of some issue");
			Extentreportutils.log(LogStatus.FAIL, "Get auto-assign logs details", "To get detailed auto-assign logs", scrrenpasspath+"autoassigndetaisl.png");
			Assert.fail("Not able to get auto assign details beacuse of some issue", e);;
	
		}
		
	}
	
	
}

