package org.TC;


	import java.io.FileInputStream;
	import java.io.IOException;
	import java.sql.Connection;
	import java.sql.SQLException;
	import java.util.Properties;
	import org.apache.log4j.Logger;
	import org.browsersetup.Browsersetup;
	import org.openqa.selenium.By;
	import org.openqa.selenium.WebDriver;
	import org.testng.Assert;
	import org.testng.annotations.AfterClass;
	import org.testng.annotations.BeforeClass;
	import org.testng.annotations.Test;
	import com.relevantcodes.extentreports.LogStatus;
	import org.swiggy.pom.CashManagement;
	import org.utility.functions.Constant;
	import org.utility.functions.Extentreportutils;
	import org.utility.functions.Login;
	import org.utility.functions.Takescreenshots;

	public class Cash_Management {


		static CashManagement CashManagement;
		static WebDriver dr;
		static Properties pro;
		static Logger log;
		static FileInputStream fis;
		static Connection con;
		static String dt;
		static boolean t;
		static String scrrenpasspath = Constant.scrrenpasspaths;

		@BeforeClass(groups = "swiggy")
		public void logintodelivery() throws IOException, InterruptedException,
				ClassNotFoundException, SQLException {
			fis = new FileInputStream(Constant.objpathcashman);
			pro = Browsersetup.loadProperties(fis);
			log = Browsersetup.logger1();
			//DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
			//Date dateobj = new Date();
			//dt = df.format(dateobj);
			dr = Browsersetup.setup("Chrome", Constant.objpathcashman,
				pro.getProperty("loginurl"));
			CashManagement = new CashManagement(dr);
			Login.logintoportal(dr, pro.getProperty("username"),
			pro.getProperty("password"), Constant.objpathcashman);

		}

		@Test(groups = "swiggy", priority = 1)
		public void findsession() throws IOException {
			try {
				Extentreportutils.starttest("Find Session",
						"To click active session or create a new session");
				// Click cash management link
				CashManagement.clickcashmanagemenlink(pro
						.getProperty("createcashlink"));
				// Enter DE id
				log.info("Clicked on cash management link");
				CashManagement.enterdeid(pro.getProperty("Deidinputbox"),
						pro.getProperty("De_id"));
				// Click find session button
				log.info("User enetered DE id " + pro.getProperty("De_id"));
				Takescreenshots.screenshots(dr, "DEsessions");
				CashManagement.clicksessionbutton(pro
						.getProperty("clicksessionbtn"));
				Extentreportutils.log(LogStatus.PASS, "Check session for DE",
						"To check all active and inactive sessions of DE",
						scrrenpasspath + "DEsessions.png");
				// Check whether session is in progress or to create new session
				log.info("User clicked on submit button");
				t = CashManagement.checksession(pro.getProperty("Session"));
				log.info("Checking session status whether session is active or not is "
						+ t);
				if (t == true) {
					// click on in progress session
					CashManagement.selectsession(pro.getProperty("clicksession"));
					log.info("User selected session");
					Assert.assertTrue(t);
				} else {
					// Click on create new session button
					CashManagement.clickcreatesessionbtn(pro
							.getProperty("createnewsession"));
					log.info("User clicked on create new session button");
					String h1 = dr.getWindowHandle();
					dr.switchTo().window(h1);
					// Enter session name
					CashManagement.entersessionname(
							pro.getProperty("entersessiontext"),
							pro.getProperty("Enter_session_name"));
					// Click create session button
					log.info("User entered session name as "
							+ pro.getProperty("Enter_session_name"));
					Takescreenshots.screenshots(dr, "createsession");
					CashManagement.clickcreatesession(pro
							.getProperty("clickcreateseessiontbn"));
					log.info("Clicked on create session button");
					Extentreportutils.log(LogStatus.PASS, "Create new session",
							"Creating new session when no active session",
							"createsession.png");
					Assert.assertTrue(!t);
				}

			} catch (Exception e) {
				System.out.println("Not able to find session");
				log.info("Not able to find session", e);
				Takescreenshots.screenshots(dr, "clicksessionfail");
				Extentreportutils.log(LogStatus.FAIL,"Find session" , "Not able to find session", scrrenpasspath+"clicksessionfail.png");
				Assert.fail("Not able to find session", e);
			}

		}

		@Test(groups = "swiggy", priority = 2)
		public void addmanualtransaction() throws InterruptedException, IOException {
			try {
				// Click Add manual Transaction button
				Extentreportutils.starttest("Add manual Transaction",
						"To add manual transaction against some order");
				CashManagement.clickaddmanualtransactionbtn(pro
						.getProperty("AddmanualTransaction"));
				log.info("Add maunal transaction button clicked");
				// Move to add manual transaction pop up
				String h1 = dr.getWindowHandle();
				dr.switchTo().window(h1);
				// select transaction type for manual transaction
				switch (pro.getProperty("select_trans_typ")) {
				case "Disburse cash":
					// Select Transaction type
					CashManagement.selecttranstype(pro
							.getProperty("select_trans_typ"));
					// Enter DE incoming amount
					log.info("User selected transaction type as "
							+ pro.getProperty("select_trans_typ"));
					CashManagement.enterdeamount(dr.findElement(By.xpath(pro
							.getProperty("EnterDeincoming"))), pro
							.getProperty("Enter_De_incoming"));
					// Enter Transaction description
					log.info("User entered DE incoming amount as "
							+ pro.getProperty("Enter_De_incoming"));
					CashManagement.enterdescription(dr.findElement(By.xpath(pro
							.getProperty("EnterDescription"))), pro
							.getProperty("Enter_Description"));
					log.info("User entered " + pro.getProperty("Enter_Description")
							+ " as description");
					Takescreenshots.screenshots(dr, "manualtransaddclick");
					Extentreportutils.log(LogStatus.PASS, "Select disburse cash",
							"To select transaction type as Disburse cash",
							scrrenpasspath + "manualtransaddclick.png");

				case "Collect cash":
					// Select Transaction type
					CashManagement.selecttranstype(pro
							.getProperty("select_trans_typ"));
					Thread.sleep(2000);
					log.info("User selected transaction type as "
							+ pro.getProperty("select_trans_typ"));
					// Enter DE outgoing amount
					CashManagement.enterdeamount(dr.findElement(By.xpath(pro
							.getProperty("EnterDeoutgoing"))), pro
							.getProperty("Enter_DE_outgoing"));
					// Enter description for manual transaction
					log.info("User enetered DE outgoing amount as "
							+ pro.getProperty("Enter_DE_outgoing"));
					CashManagement.enterdescription(dr.findElement(By.xpath(pro
							.getProperty("EnterDescription"))), pro
							.getProperty("Enter_Description"));
					log.info("User enetered "
							+ pro.getProperty("Enter_Description")
							+ " as description");
					Takescreenshots.screenshots(dr, "manualtransaddclick");
					Extentreportutils.log(LogStatus.PASS, "Select collect cash",
							"To select transaction type as collect cash",
							scrrenpasspath + "manualtransaddclick.png");

				case "Order related transaction":
					// Select Transaction type
					CashManagement.selecttranstype(pro
							.getProperty("select_trans_typ"));
					log.info("User selected transaction type as "
							+ pro.getProperty("select_trans_typ"));
					// Enter Order id
					CashManagement.enterorderid(pro.getProperty("Enterordrid"),
							pro.getProperty("Enter_order_id"));
					// Enter DE incoming amount for manual transaction
					log.info("User adding manual transaction for order id "
							+ pro.getProperty("Enter_order_id"));
					CashManagement.enterdeamount(dr.findElement(By.xpath(pro
							.getProperty("EnterDeincoming"))), pro
							.getProperty("Enter_De_incoming"));
					// Enter DE outgoing amount for manual transaction
					log.info("User entered DE incoming amount as "
							+ pro.getProperty("Enter_De_incoming"));
					CashManagement.enterdeamount(dr.findElement(By.xpath(pro
							.getProperty("EnterDeoutgoing"))), pro
							.getProperty("Enter_DE_outgoing"));
					// Enter Description for manual transaction
					log.info("User entered DE outgoing amount as "
							+ pro.getProperty("Enter_DE_outgoing"));
					// Enter Description
					CashManagement.enterdescription(dr.findElement(By.xpath(pro
							.getProperty("EnterDescription"))), pro
							.getProperty("Enter_Description"));
					log.info("User entered " + pro.getProperty("Enter_Description")
							+ " as description");
					Takescreenshots.screenshots(dr, "manualtransaddclick");
					Extentreportutils.log(LogStatus.PASS,
							"Select order related transaction",
							"To select transaction type as order related",
							scrrenpasspath + "manualtransaddclick.png");

				}
				CashManagement
						.clickaddtrnasbtn(pro.getProperty("clickaddtransbtn"));
				log.info("User clicked on add transaction button");
				String order = pro.getProperty("Enter_order_id");
				String query = "Select * from de_cash_order_txn_session "
						+ "where order_id=" + order + " order by 1 desc limit 1";

				boolean b = CashManagement.checktransadded(query,
						pro.getProperty("De_id"), "DASHBOARD");
				if (b == true) {
					Takescreenshots.screenshots(dr, "manualtransadded");
					Extentreportutils.log(LogStatus.PASS, "Add Manual transaction",
							"To add manual transaction", scrrenpasspath
									+ "manualtransadded.png");
					Assert.assertTrue(b);
				} else {
					Takescreenshots.screenshots(dr, "manualtransaddedfail");
					Extentreportutils.log(LogStatus.FAIL, "Add Manual transaction",
							"To add manual transaction", scrrenpasspath
									+ "manualtransaddedfail.png");

					log.debug("Add manual transaction test case failed");
					Assert.fail("Add manual transaction failed");
					;

				}
			} catch (Exception e) {
				System.out
						.println("Not able to add manual transaction beacuse some error occured");
				log.debug(
						"Not able to add manual transaction because some error occured",
						e);
				Takescreenshots.screenshots(dr, "manualtransaddedfail");
				Extentreportutils.log(LogStatus.FAIL, "Add Manual transaction",
						"To add manual transaction", scrrenpasspath
								+ "manualtransaddedfail.png");

				Assert.fail(
						"Not able to add manual transaction because some error occured",
						e);
			}

		}

		@AfterClass(groups = "swiggy")
		public void closebrowser() {
			if (dr != null) {
				System.out.println("Closing the browser");
				dr.quit();
			}
		}
	}
